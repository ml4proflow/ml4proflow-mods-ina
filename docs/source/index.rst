.. ml4proflow documentation master file, created by
   sphinx-quickstart on Fri Feb  4 12:55:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ml4proflow\'s documentation!
=======================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

.. include:: ../../README.md
   :parser: myst_parser.sphinx_

Example Workflows
==================

.. nbgallery::
   ../../notebooks/data_segmentation.ipynb

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
