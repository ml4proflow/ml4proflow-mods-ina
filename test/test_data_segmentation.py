import unittest

import numpy as np
import pandas as pd

from ml4proflow_mods.ina.data_segmentation.data_segmentation import DataSegmentation


class TestDataSegmentation(unittest.TestCase):
    def setUp(self) -> None:
        x = np.linspace(0, 250, num=250)
        data = pd.DataFrame(data=np.sin(x), index=None, columns=['A'])
        data['B'] = np.sin(x*2)
        self.segmenter = DataSegmentation(dataframe=data)
        self.segmenter.set_training_data(['A'])

    def test_find_periodicities(self):
        periodicities, ratings = self.segmenter.find_periodicities(method='FFT', method_params={})
        assert periodicities == [6, 13, 19, 25, 31, 44, 69, 63, 56, 94, 81, 119, 113, 106]
        assert ratings == (1.6906, 1.113, 1.068, 1.0546, 1.0481, 1.0433, 1.04, 1.04, 1.04, 1.0388, 1.0388, 1.038, 1.038,
                           1.038)

    def test_find_peaks(self):
        peaks = self.segmenter.find_peaks(0, 0)
        assert peaks == [2, 8, 14, 20, 27, 33, 39, 45, 52, 58, 64, 70, 77, 83, 89, 95, 102, 108, 114, 120, 127, 133,
                         139, 145, 152, 158, 164, 171, 177, 183, 189, 196, 202, 208, 214, 221, 227, 233, 239, 246]

    def test_pattern_handling(self):
        exp_patterns = self.segmenter.identify_patterns(5, 3)

        assert len(exp_patterns.keys()) == 3
        assert exp_patterns[0].shape[0] == 5

        self.segmenter.set_pattern(exp_patterns[0])
        self.segmenter.find_pattern()
        res_df = self.segmenter.mark_in_data('patterns')

        assert 'period' in res_df.columns.tolist()


if __name__ == '__main__':
    unittest.main()
