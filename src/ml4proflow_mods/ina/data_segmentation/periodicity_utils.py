import numpy as np
import pandas as pd
import scipy.signal
import scipy.cluster.vq


def mark_periodicity(data: pd.DataFrame, periodicity: int) -> pd.DataFrame:
    """Takes input dataframe and input periodicity number and adds extra column called period with increasing number
    representing the process iteration.

    Args:
        data: pandas dataframe containing the data.
        periodicity: Integer representing the passed data points between two process iterations.

    Returns:
        The input dataframe with the added column period containing an increasing number pertaining to the process
        iteration.
    """
    n_rows = data.shape[0]
    periods = np.zeros(n_rows)

    counter = 0
    for i in range(0, n_rows, periodicity):
        periods[i:i + periodicity] = counter
        counter += 1
    periods[counter * periodicity:] = -1

    data['period'] = periods
    return data


def find_peaks_modified(data: np.ndarray, peak_distance: float, peak_height: float) -> [float]:
    """This function calls the scipy signal function find peaks with the parameters peak_distance and peak_height.

    If peak_distance and peak_height both equal 0, the defaults parameters are used.

    Args:
        data: A numpy array containing the signal, which is to be searched for peaks.
        peak_distance: The minimal distance between two peaks.
        peak_height: The minimal height of a peak.

    Returns:
        A list containing the identified peaks in the signal.
    """
    if peak_distance == 0 and peak_height == 0:
        peaks, _ = scipy.signal.find_peaks(data.flatten())
    elif peak_distance == 0:
        peaks, _ = scipy.signal.find_peaks(data.flatten(), height=peak_height)
    elif peak_height == 0:
        peaks, _ = scipy.signal.find_peaks(data.flatten(), distance=peak_distance)
    else:
        peaks, _ = scipy.signal.find_peaks(data.flatten(), distance=peak_distance, height=peak_height)

    list_peaks = peaks.tolist()
    return list_peaks


def slice_by_peaks(data: pd.DataFrame, list_peaks: []) -> [pd.DataFrame]:
    """Takes data and a list of peaks and returns a list of new dataframes whose slices pertain to the location of the
    peaks.

    Args:
        data: Pandas dataframe containing the data which is to be sliced.
        list_peaks: A list of peak locations.

    Returns:
        A list of pandas dataframes, sliced by the locations of the peaks.
    """
    return [data.iloc[list_peaks[n]:list_peaks[n + 1]] for n in range(len(list_peaks) - 1)]


def mark_by_peaks(data: pd.DataFrame, list_peaks: []) -> pd.DataFrame:
    """This function adds a column called period which increments everytime a peak location is passed.

    Args:
        data: A pandas dataframe, where the column is added.
        list_peaks: List of peak locations.

    Returns:
        A pandas dataframe with the added column period representing the passing of a identified peak in the data.
    """
    periods = np.full(data.shape[0], -1)
    counter = 0
    for i in range(0, len(list_peaks)-1):
        start = list_peaks[i]
        stop = list_peaks[i+1]
        if start < 0:
            stop = stop - start
            start = 0
        if stop > len(data):
            stop = len(data)
        periods[start:stop] = counter
        counter += 1

    data['period'] = periods
    return data


def find_pattern(pattern: np.ndarray, signal: np.ndarray, step_size: int = 10, percentile: float = 0.05,
                 prom: float = None, padding: bool = False): # -> tuple[[], np.array, float]: (not supported <3.9)
    """This function takes a pattern and a signal and outputs indices indicating the occurrence of this pattern.

    For this purpose, the pattern is correlated with the signal, peaks indicate the occurrence of this pattern.

    Args:
        pattern: Numpy array containing the pattern.
        signal: Numpy array containing the signal.
        step_size: Step size of the correlation, smaller is more precise, but takes longer to calculate.
        percentile: Only patterns of a similarity of (1 - percentile) * 100 per cent are considered.
        prom: Only correlations with a certain prominence are regarded.
        padding: To also identify patterns in the beginning, the signal is padded with one pattern length.

    Returns:
        The locations of the patterns, the calculated correlations and the maximum prominence.
    """
    if not padding:
        data = signal
        correlations = np.zeros(data.shape[0])

        for i in range(0, data.shape[0]-pattern.shape[0], step_size):
            correlations[i] = np.correlate(data[i:(i+pattern.shape[0])], pattern)

        if not prom:
            prom = np.max(correlations) - (np.max(correlations)*percentile)

        peaks, _ = scipy.signal.find_peaks(correlations, distance=int(pattern.shape[0]), height=prom)
        return peaks, correlations, prom
    else:
        padder = np.zeros(len(pattern))
        data = np.hstack((padder, signal, padder)).ravel()
        correlations = np.zeros(data.shape[0])

        for i in range(0, data.shape[0]-pattern.shape[0], step_size):
            correlations[i] = np.correlate(data[i:(i+pattern.shape[0])], pattern)

        if not prom:
            prom = np.max(correlations) - (np.max(correlations)*percentile)

        peaks, _ = scipy.signal.find_peaks(correlations, distance=int(pattern.shape[0]))
        peaks = [peak - len(pattern) for peak in peaks]

        tmp_peaks = []
        for peak in peaks:
            if peak < 0:
                tmp_peaks.append(0)
            else:
                tmp_peaks.append(peak)
        peaks = tmp_peaks

        return peaks, correlations, prom


def mark_pattern(data: pd.DataFrame, peaks: [], pattern: []) -> pd.DataFrame:
    """This function adds the column period to the data, where each incrementing number represents the occurrence of the
    pattern in the data.

    -1 means no occurrence at this point in time.

    Args:
        data: Pandas dataframe containing the data, where the column is added.
        peaks: A list of indices of pattern occurences from the function find_pattern().
        pattern: The actual pattern that was searched.

    Returns:
        The dataframe with the added column.
    """
    pattern_len = len(pattern)

    n_rows = data.shape[0]
    periods = np.full(n_rows, -1)

    counter = 0
    for i in peaks:
        start = i
        stop = i + int(pattern_len)
        if start < 0:
            stop = stop - start
            start = 0
        if stop > n_rows:
            stop = n_rows
        periods[start: stop] = counter
        counter += 1

    data['period'] = periods
    return data


def pattern_recommender(pattern_length: int, signal: np.ndarray, num_patterns: int = 5, start_with_zero: bool = False)\
        -> {}:
    """This function searches for patterns of a certain length in a given signal by examining the deviation from zero.

    Args:
        pattern_length: The length of patterns that are searched.
        signal: The one dimensional signal, where the patterns are searched in.
        num_patterns: The number of types of different patterns in which the found patterns are clustered.
        start_with_zero

    Returns:
        A dictionary containing one pattern per type.
    """
    signal = np.array(signal).astype(float)
    moving_avg = np.zeros(signal.shape[0])

    for i in range(0, signal.shape[0]-pattern_length):
        moving_avg[i] = np.average(signal[i:(i+pattern_length)])

    moving_avg_scaled = np.abs(moving_avg)

    # Find peaks and cluster by prominence and height
    peaks, _ = scipy.signal.find_peaks(moving_avg_scaled, distance=int(pattern_length))
    prominences, left_bases, right_bases = scipy.signal.peak_prominences(moving_avg_scaled, peaks)
    peak_heights = np.array([moving_avg_scaled[x] for x in peaks])

    cluster_data = np.array(np.stack((peak_heights, prominences), axis=1))
    cluster_data_whitened = scipy.cluster.vq.whiten(cluster_data)
    codebook, distortion = scipy.cluster.vq.kmeans(cluster_data_whitened, num_patterns)
    vq_data_code, dist = scipy.cluster.vq.vq(cluster_data_whitened, codebook)

    # Go through peaks and return one example peak per cluster
    vq_data_code = vq_data_code.tolist()
    patterns = dict()
    for value in set(vq_data_code):
        idx = vq_data_code.index(value)
        patterns[value] = signal[peaks[idx]: peaks[idx]+pattern_length]
        if start_with_zero:
            peak = peaks[idx]
            zero_counter = 0
            for i in range(0, int(pattern_length/2)):
                peak = peak-i
                if peak < 0:
                    break
                if 0.1 > signal[peak] > -0.1:
                    zero_counter += 1
                else:
                    zero_counter = 0
                if zero_counter == 1:
                    patterns[value] = signal[peak: peak+pattern_length]
                    break

    return patterns

