import numpy as np
from scipy.fftpack import fft, ifft
from scipy.spatial.distance import euclidean


class periodictiyRater:
    '''
    This class is used to rate found periods in a timeseries signal
    '''

    def cost_function_periodicity(self, s, period, ret_reconstrut=False):
        # 1. FFT von einer bekannten Periode
        _mu = np.mean(s)
        _peridod_fft = fft(s[0:period])

        # 2. Rekonstruktion des gesamten Signals durch FFT
        rc_signal = ifft(_peridod_fft, n=len(s)) + _mu

        # 3. Euklidische Distanz von Orignal und Rekonstruktion
        cost = euclidean(s, rc_signal)
        if ret_reconstrut:
            return cost, rc_signal
        return cost

    def compare_periodlists(self, ref_list, list_b, tolerance=10):
        print("COMPARE")
        rank_discplace_list = []
        close_list = []
        missing_list = []
        print("List B: {}".format(list_b))
        print("Ref List: {} \n".format(ref_list))

        hit_list = np.zeros(len(ref_list))

        for x, obs in enumerate(list_b):
            # check exact occurence
            if obs in ref_list:
            #     print("Is in list!")
                ref_place = np.where(np.asarray(ref_list) == obs)[0][0]
                rank_discplace_list.append(np.abs(ref_place - x))
            else:
                rank_discplace_list.append(-1)

            # check dist to nearest
            nearest = min(ref_list, key=lambda x: abs(x - obs))
            close_list.append(np.abs(nearest - obs))
            near_idx = np.where(np.asarray(ref_list) == obs)
            hit_list[near_idx] += 1

        print("Rank Displace List:")
        print(rank_discplace_list)
        print("Closest List:")
        print(close_list)
        print("Hit list:")
        print(hit_list)
        print("\n")

