import numpy as np
import time
from statsmodels import api as sm


class AutoCor4Period:
    """Class finds periodicities in discrete data using autocorrelation.
    """

    def __init__(self, period_range_tolerance: int = 1, verbose: int = 0, plot_path: str = None,
                 extra_folder: bool = True, filter_periods: bool = True) -> None:
        """Initializes the AutoCor for Period class.

        Args:
            period_range_tolerance: Parameter for filtering a range sorted period list in method _filter_ranges().
            verbose: 0 if no printouts are done, 1 id basic level printouts should be done.
            plot_path: If path is set, plots are generated and saved there.
            extra_folder: If is set to True, an extra folder called fft is created for saving the plots.
            filter_periods: If is set to True, found periodicities are filtered to not display multiples.

        """
        self.period_tolerance = period_range_tolerance
        self.train_data = None
        self.verbose = verbose
        self.plot_path = plot_path
        self.extra_folder = extra_folder
        self.filter_periods = filter_periods

    def findPeriods(self, inputTSdata, ret_acf: bool = False) -> []:
        """

        Args:
            inputTSdata:
            ret_acf:

        Returns:
            List of found periodicities and list of corresponding ratings. If ret_acf is True, it is returned as a third
            parameter.
        """
        self.train_data = inputTSdata

        if self.verbose > 0:
            print("\t...calculating acf")
        acf = sm.tsa.acf(inputTSdata, nlags=len(inputTSdata), fft=False)

        _half = int(len(inputTSdata)/2)

        if self.verbose > 0:
            print("\t...search for peaks")
        _idx_peaks, _val_peaks = self._get_peaks(acf, _half)
        if _val_peaks == []:
            Warning('No peaks in acf found. Please check input values.')
            if ret_acf:
                return [], [], acf
            else:
                return [], []
        if self.verbose > 0:
            print("\t...filter")
        if self.filter_periods:
            _idx_peaks = self.filter_mulitples(_idx_peaks)
            _idx_peaks = self._filter_ranges(_idx_peaks)

        if self.verbose > 0:
            print("\t...rating")

        _idx_peaks, rating = self._rate_periods_fft(_idx_peaks, inputTSdata)

        if ret_acf:
            return _idx_peaks, rating, acf
        else:
            return _idx_peaks, rating

    def _rate_periods_fft(self, period_list: [], inputTSdata) -> [[int], [int]]:
        """Given a list of potential periodicities and the original time series data, calculates corresponding ratings
        based on ft peak heights.

        Args:
            period_list:
            inputTSdata:

        Returns:
            List of found periodicities and list of corresponding ratings.
        """
        _input_fft = np.sqrt(np.power(np.fft.fft(inputTSdata, axis=0), 2))
        freq = np.fft.fftfreq(len(inputTSdata), 0.001)

        val_list = []

        for x, per in enumerate(period_list):
            # get frequencies of period
            _f = (1000 / per)

            # get index
            _plot_idx = (np.where(freq > _f)[0])[0] * 2

            # find nearest in fft
            _fft_idx = int(np.argmin(np.abs(freq[0:_plot_idx] - _f)))

            # get value of _fft_idx
            val = _input_fft.real[_fft_idx]

            #normalize by max value of fft
            val = float(np.round(val/np.max(_input_fft.real[1:-1])*100, 4))

            val_list.append(val)

        val_list, period_list = list(zip(*sorted(zip(val_list, period_list), reverse=True)))

        period_list = list(period_list)

        return period_list, val_list

    def filter_mulitples(self, period_list: []) -> []:
        """Multiple values are erased from a list of possible periods.

        Args:
            period_list: List of periods to check for multiples.
        Returns:
            A clean list of simples.
        """
        periods = list(np.sort(period_list))
        simples = []

        for x, per in enumerate(periods):
            if x == 0:
                simples.append(per)
            else:
                check = True
                for s in simples:
                    if per % s == 0:
                        check = False
                        break
                if check:
                    simples.append(per)
        return simples

    def _filter_ranges(self, period_list: []) -> []:
        """Filtering the periods by + and - tolerance to extract ranges of possible periods. This method is based on
        the assumption that the period list is presorted by relevance. The ranges a stored internal within the class.

        Args:
            period_list: List of found periodicities.
        Returns:
            Filtered period_list.
        """
        filterlist = []
        self.period_range_dict = {}

        for x, per in enumerate(period_list):
            run = True
            cnt = 1
            tol_cnt = 0
            while run:
                try:
                    a = period_list.index(per + cnt + tol_cnt)
                    del period_list[a]
                    cnt += 1
                except ValueError:
                    if tol_cnt >= self.period_tolerance:
                        self.period_range_dict[per] = cnt + tol_cnt
                        run = False
                    else:
                        tol_cnt += 1
            filterlist.append(per)
        return filterlist

    def _get_peaks(self, values, half) -> [[], []]:
        """

        Args:
            values: Array like object of a time series signal to search.
            half:

        Returns:
            A list of Indices of the peaks and a list of the values of the peaks.
        """
        peaks = []
        peak_values = []
        derv_values = []

        derv1 = np.diff(values)
        last = derv1[0]

        #get all slope switches
        for x, d in enumerate(derv1):
            if last >= 0:
                if d < 0:
                    last = d
                    peaks.append(x)
                    derv_values.append(d)
                    peak_values.append(values[x])
            else:
                if d >= 0:
                    last = d

        ############### post processing ##############
        # Disregard everything above half
        _too_much = np.where(np.asarray(peaks) <= half)[0]
        peaks = [peaks[x] for x in _too_much]
        peak_values = [peak_values[x] for x in _too_much]

        # Search for multiples (shouldnt be there)
        # Find k best
        ##############################################
        if peaks != []:
            # Sorting the peaks by value
            peak_values, peaks = list(zip(*sorted(zip(peak_values, peaks), reverse=True)))
            peak_values = list(peak_values)
            peaks = list(peaks)

            # Erase those with negative ACF values
            negatives = list(np.where(np.asarray(peak_values) <= 0)[0])
            negatives.reverse()

            for n in negatives:
                peak_values.pop(n)
                peaks.pop(n)
            return peaks, peak_values
        else:
            return [], []

    def _rate_periods(self, period_list):
        if self.verbose > 1:
            time_sum = 0
            t0 = time.time()

        self.train_data = np.asarray(self.train_data)

        sorted_dist_List = []

        if self.train_data is not None:
            for x, p in enumerate(period_list):
                _cuts = None
                for cut_start in range(0, len(self.train_data), p):
                    if len(self.train_data) - (cut_start+p) >= 0:
                        if _cuts is None:
                            _cuts = self.train_data[cut_start:cut_start+p]
                        else:
                            # print("len: ", len(self.train_data[cut_start:cut_start+p]))
                            _cuts = np.hstack((_cuts, self.train_data[cut_start:cut_start+p]))
                if self.verbose > 1:
                    t1 = time.time()
                    td = t1 - t0
                    time_sum += td
                    eta = int((len(period_list) - x) * (time_sum/(x+1)))
                    print("\t\t rating period {} of {}, t elapsed: {}s, eta: {}s".format(x,
                                                                                         len(period_list),
                                                                                         np.round(td, 0),
                                                                                         eta))
                    t0 = t1

                d = self.dist_check_C(_cuts)

                sorted_dist_List.append(d)

            sorted_dist_List, period_list = list(zip(*sorted(zip(sorted_dist_List, period_list), reverse=False)))
            period_list = list(period_list)
        else:
            Warning("Please use the fit function")
        return period_list

    def dist_check_C(self, mat):
        a = []
        for row in mat:
            _ = sum((x - y) ** 2 for (x, y) in zip(row, mat)) ** 0.5
            a.append(_)
        return np.sum(a)/mat.shape[1]
