from __future__ import print_function, annotations
import pandas
import numpy as np

from ml4proflow.modules import Module, DataFlowManager, SourceModule
from . import data_segmentation


class DataSegmentationModule(Module):
    def __init__(self, dfm: DataFlowManager, config):
        Module.__init__(self, dfm, config)
        self.segmenter = data_segmentation.DataSegmentation()
        self.config = config
        self.config.setdefault('mode', 'Online')
        self.config.setdefault('push_options', 'Whole dataframe')
        self.config.setdefault('process_config', 'config.json')
        self.config_range['push_options'] = ['Whole dataframe', 'Slices']
        self.config_desc['push_options'] = 'Either Push whole dataframe or slices individually.'
        self.config_range['mode'] = ['Training', 'Online']

    def on_new_data(self, name: str, sender: SourceModule, data: pandas.DataFrame):
        if self.config['mode'] == 'Online':
            if self.config['process_config'] is not None:
                self.segmenter = data_segmentation.DataSegmentation()
                self.segmenter.set_data(data)
                self.segmenter.set_config(self.config['process_config'])
                result = self.segmenter.run()

                tmp_attrs = result.attrs
                if 'chain' in tmp_attrs:
                    tmp_attrs['chain'].append('data_segmentation')

                if self.config['push_options'] == 'Whole dataframe':
                    for channel in self.config['channels_push']:
                        self._push_data(channel, result.reset_index(drop=True))
                    return

                unique_patterns = np.sort(result['period'].unique())
                for pattern_number in unique_patterns:
                    if pattern_number == -1:
                        continue
                    tmp_result = result.loc[result['period'] == pattern_number].copy()
                    tmp_result.attrs = tmp_attrs
                    tmp_result.drop(['period'], axis='columns', inplace=True)

                    if 'source_name' in tmp_result.attrs:
                        tmp_result.attrs['sink_name'] = tmp_result.attrs['source_name'].split('.')[0] + '_' + str(
                            pattern_number) + '.' + tmp_result.attrs['source_name'].split('.')[1]
                    else:
                        tmp_result.attrs['sink_name'] = 'occurance_' + str(pattern_number)
                    tmp_result.attrs['segment'] = pattern_number
                    if tmp_result.empty:
                        # self.update_status('Keine Periode gefunden')
                        pass
                    else:
                        for channel in self.config['channels_push']:
                            self._push_data(channel, tmp_result.reset_index(drop=True))
            else:
                raise ValueError('Please submit valid config.')
        elif self.config['mode'] == 'Training':
            self.segmenter.set_data(data)
        else:
            raise ValueError('Mode must be run or configuration.')

    @classmethod
    def get_module_desc(cls):
        return {"name": "Data Segmentation",
                "categories": ["Preprocessing"],
                "jupyter-gui-cls": "ml4proflow_mods.ina.widgets.data_segmentation_widget.DataSegmentationWidget",
                "jupyter-gui-override-settings-type": {'process_config': 'file'},
                "html-description": """
                <H1 style="margin-bottom:6px;margin-top:6px">Datensegmentierung</H1>
                <h2 style="margin-bottom:1pt">Kurzbeschreibung:   <button type="button">Handbuch</button></h2><p>Die Datensegmentierungsverfahren dienen der Vorverarbeitung der erhobenen Daten für die Weiterverarbeitung in Maschinenlernverfahren. Vorverarbeitungsschritte beinhalten Skalierung, Dimensionsreduktion und Filter. Die Segmentierung erfolgt durch die Auswahl von interessanten Mustern oder Periodizitäten durch den Nutzer.</p><h2 style="margin-bottom:1pt">Nutzen:</h2><p>Die Datensegmentierung unterstützt dabei Daten in eine geeignete Form für die Weiterverarbeitung mit Maschinellen Lernverfahren zu bringen. Dies kann die Performance der angewendeten Verfahren verbessern.</p>
                <h2 style="margin-bottom:1pt">Geeignete Maschinentypen:</h2><ul><li>Bisher keine Einschränkungen bekannt.</li></ul>
                """
                }
