import re
import numpy as np
import pandas as pd
from itertools import product
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA


class RegExp4Period:
    """This class searches for periodicities in discrete data using regular expressions

    """
    def __init__(self, min_period: int, n_segments: int, range_tolerance: int = 0, verbose: bool = False) -> None:
        """Initializes this periodicity finder.

        Args:
            min_period: Minimum number of periods a string should appear in the data.
            n_segments: Number of segments the data should be discretized to.
            range_tolerance: Indicates how much tolerance is allowed when searching for periodicities.
            verbose: Enables print statements.
        """
        if min_period < 2:
            if verbose:
                print('Warning! Algorithm performance rises with min_period! Setting min_period on 2.')
            min_period = 2

        self.min_period = min_period
        self.n_segments = n_segments
        self.discretizer = Discretizer(self.n_segments)
        self.range_tolerance = range_tolerance
        self.verbose = verbose

    def find_periods(self, data: list or np.array) -> pd.DataFrame:
        """Searches for recurring periodicities in given data and writes them to result.csv.

        Args:
            data: Input time series in which periodicities should be found.

        Returns:
            Pd.dataframe containing found periodicities and further information.
        """
        if self.verbose:
            print('The shape of the data is {}'.format(str(data.shape)))

        if isinstance(data, list):
            data = np.asarray(data)

        # generate string in which periodicity is searched (usually the discretized data)
        goal_strings = {}
        int_discrets = {}

        # Discretize
        for column in range(0, data.shape[1]):
            goal_strings[column] = self.discretizer.discretize(data[:, column])
            int_discrets[column] = list(map(int, goal_strings[column]))

        # Compose final string using spaces
        goal_string = ''
        for counter in range(0, len(goal_strings[0])):
            for key in range(0, len(goal_strings.keys())):
                goal_string = goal_string + str(goal_strings[key][counter])
            goal_string = goal_string + ' '

        result = pd.DataFrame(columns=['symbol', 'interval', 'n_periods', 'span', 'span_start', 'span_stop', 'data_length', 'rating'])

        # words are all possible combinations of symbols
        symbols = set(goal_string)
        symbols.discard(' ')
        words = list(map(lambda x: ''.join(x), [w for w in product(symbols, repeat=data.shape[1])]))

        for x, searchString in enumerate(words):
            if self.verbose:
                print("{}/{}".format(x, len(words)-1))

            # search for recurring patterns using regular expressions
            # iterate through the segments of data of length min_period
            for const in range(int(np.floor((len(goal_string) / (data.shape[1] + 1)) / self.min_period))):
                c = const
                a = searchString

                #if self.range_tolerance == 0:
                #    local_search_string = '(' + a + '(\d{3} ){' + str(c) + '}){' + str(self.min_period) + ',}' + a + ' '
                #else:
                local_search_string = '(' + a + ' (\d{3} ){' + str(c - self.range_tolerance) + ',' + str(c + self.range_tolerance) + '}){' + str(self.min_period) + ',}' + a + ' '
                if self.verbose:
                    print('SearchString1 = {}'.format(local_search_string))

                match = re.search(local_search_string, goal_string)

                if match:
                    # Try to increase minPeriod
                    max_min_period = self.min_period + 1
                    while match:
                        curr_match = match
                        max_min_period += 1

                        #if self.range_tolerance == 0:
                        #    local_search_string = '(' + a + '(\d{3} ){' + str(c) + '}){' + str(max_min_period) + ',}' + a + ' '
                        #else:
                        local_search_string = '(' + a + ' (\d{3} ){' + str(c - self.range_tolerance) + ',' + str(c + self.range_tolerance) + '}){' + str(max_min_period) + ',}' + a + ' '

                        if self.verbose:
                            print('SearchString2 = {}'.format(local_search_string))
                        match = re.search(local_search_string, goal_string)

                    span = curr_match.span()[1] - curr_match.span()[0]
                    result = result.append(
                        {'symbol': searchString, 'interval': const + 1, 'n_periods': max_min_period - 1,
                         'span': span / (data.shape[1] + 1),
                         'span_start': curr_match.span()[0] / (data.shape[1] + 1),
                         'span_stop': curr_match.span()[1] / (data.shape[1] + 1),
                         'data_length': (len(goal_string) / (data.shape[1] + 1))}, ignore_index=True)

        period_ratings = self._rate_periods_fft(self._get_intervals(result), data)

        # Add period ratings to result
        for counter_per in range(0, len(period_ratings[0])):
            result.loc[result['interval'] == period_ratings[0][counter_per], 'rating'] = period_ratings[1][counter_per]

        return result

    @staticmethod
    def _get_intervals(result: pd.DataFrame) -> list:
        return list(set(result['interval']))

    @staticmethod
    def _rate_periods_fft(period_list, data):
        """Rate periods by using fft and cosnidering the peak height.

        Args:
            period_list: Found periodicities in the data.
            data: The original data.
        """
        # If data multidim: do PCA
        if data.shape[1] > 1:

            scaler = StandardScaler(with_std=False)
            train_data = scaler.fit_transform(data)
            # scale to range 0 - 1
            scaler = MinMaxScaler()
            train_data = scaler.fit_transform(train_data)
            reducer = LDR()
            reducer.set_dim(1)
            reducer.perform_dr(train_data)
            data = list(reducer.model_points)

        _input_fft = np.sqrt(np.power(np.fft.fft(data, axis=0), 2))
        freq = np.fft.fftfreq(len(data), 0.001)

        val_list = []

        for x, per in enumerate(period_list):
            if per < 5:
                val_list.append(0)
                continue
            # get frequencies of period
            _f = (1000 / per)

            # get index
            _plot_idx = (np.where(freq > _f)[0])[0] * 2

            # find nearest in fft
            _fft_idx = int(np.argmin(np.abs(freq[0:_plot_idx] - _f)))

            # get value of _fft_idx
            val = _input_fft.real[_fft_idx]

            #normalize by max value of fft
            val = float(np.round(val/np.max(_input_fft.real[1:-1])*100, 4))

            val_list.append(val)

        val_list, period_list = list(zip(*sorted(zip(val_list, period_list), reverse=True)))

        period_list = list(period_list)

        return period_list, val_list

    def _rate_periods_fft_multidim(self, period_list, data):
        # TODO: fit for multidim data
        # _input_fft = np.sqrt(np.power(np.fft.fft(inputTSdata, axis=0), 2))
        _input_fft = np.sort(np.power(np.fft.fftn(data), 2))

        freq = np.fft.fftfreq(data.shape[0], 0.001)

        val_list = []

        for x, per in enumerate(period_list):
            if per < 5:
                val_list.append(0)
                continue

            # get frequencies of period
            _f = (1000 / per)

            # get index
            _plot_idx = (np.where(freq > _f)[0])[0] * 2

            # find nearest in fft
            _fft_idx = int(np.argmin(np.abs(freq[0:_plot_idx] - _f)))

            # get value of _fft_idx
            val = _input_fft.real[_fft_idx]

            # TODO: what is best metric?
            val = np.mean(val)

            #normalize by max value of fft
            val = float(np.round(val/np.max(_input_fft.real[1:-1])*100, 4))

            val_list.append(val)

        val_list, period_list = list(zip(*sorted(zip(val_list, period_list), reverse=True)))

        period_list = list(period_list)

        return period_list, val_list


class Discretizer:
    """Class discretizes data using quantils.

    """
    def __init__(self, n_quantils: int) -> None:
        """Initializes the discretizer.

        Args:
            n_quantils: Number of quantils, to seperate data into.

        """
        self.n_quantils = n_quantils

    def discretize(self, data):
        """Discretizes given data into quantils.

        Args:
            data: Data that shall be discretized.

        Returns:
            Discret string (e.g. 00111222221101112) indicating in which quantil data point is located.

        """
        q = np.zeros(self.n_quantils)
        for i in range(self.n_quantils):
            q[i] = np.quantile(data, (i+1)/self.n_quantils)

        # discretize data
        discret = []

        for a in data:
            found = False
            for i in range(self.n_quantils-1):
                if q[i + 1] >= a > q[i]:
                    discret.append(i+1)
                    found = True
            if not found:
                discret.append(0)

        # convert ints in discrete to string
        str_discret = ''

        for d in discret:
            str_discret += str(d)

        return str_discret


class LDR:
    """Class implements dimension reduction using principal component analysis.
    """
    def __init__(self, dim_red: int = 3) -> None:
        """Initializes the model

        Args:
            dim_red: Dimension the data is reduced to

        """
        self._dim = dim_red
        self.dr = None
        self.model_points = None

    def transform_data(self, data_vector: np.ndarray) -> np.ndarray:
        """Transforms the given data into the required form.

        Args:
            data_vector: Ndarray of data which shall be transformed.

        Returns:
            Ndarray of transformed data.

        """
        return self.dr.transform(data_vector.reshape(1, -1)).reshape(1, self._dim)

    def perform_dr(self, data_matrix: np.ndarray) -> None:
        """Reduces the dimensions of the given data matrix to the chosen ones.

        Args:
            data_matrix: Ndarray of data which shall be reduced.

        """
        self.dr = PCA(n_components=self._dim)
        self.dr.fit(data_matrix)
        self.model_points = self.dr.transform(data_matrix)

    def set_dim(self, d: int) -> None:
        """Setter method for the dimension, the data is reduced to.

        Args:
            d: The new dimension the data is reduced to.

        """
        self._dim = d
