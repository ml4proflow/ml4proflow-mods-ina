from __future__ import print_function
import numpy as np
import pandas as pd
from pathlib import Path
import sys
import os
import copy

from . import base_process
from . import periodicity_utils
from . import autocor_4_period
from . import reg_exp_4_period


class DataSegmentation(base_process.BaseProcess):
    def __init__(self, dataframe: pd.DataFrame = None, config: {} = None):
        super().__init__(dataframe, config)
        self.train_data = None
        self.periodicities = None
        self.pattern = None
        self.pattern_counter = 0
        # Config to temporarily save patterns, can be added to config via save_to_config() function
        self.tmp_config = {}

    def set_config(self, config: str or {}) -> None:
        """Takes either a dictionary or a path and converts it into right format for setting this config.

        Args:
            config: Either a String containing the path to a json or a dictionary directly.
        """
        super(DataSegmentation, self).set_config(config)

        # Return Pattern to right format
        for key in self.config.keys():
            if self.config[key]['mark_in_data']['mark'] == 'patterns':
                self.config[key]['mark_in_data']['mark_params']['pattern'] = \
                    np.expand_dims(self.config[key]['mark_in_data']['mark_params']['pattern'], -1)

    def get_data_header(self) -> []:
        """Function returns list of column name in data.

        Returns:
            List of column names in data.
        """
        return self.data.columns.values.tolist()

    def set_training_data(self, column_list: []) -> None:
        """Method takes a list of column names and sets self.train_data to a ndarray containing the column data.

        Args:
            column_list: A list of column names which are to be processed.
        """
        self.tmp_config['set_training_data'] = {'column_list': column_list}
        column_idx = [list(self.data.columns.values).index(element) for element in column_list]
        self.train_data = self.data.to_numpy()[:, column_idx]

    def find_periodicities(self, method: str = None, method_params: {} = None):
        """This method applies either the FFT or REGEX method for finding periodicities in the training data.

        Args:
            method: String defining the method, can either be FFT or REGEX.
            method_params: A dictionary containing additional parameters depending on the method.
        Returns:
            The found periodicities and optionally ratings.
        """
        if method not in ['FFT', 'REGEX']:
            raise ValueError('Please use either FFT or REGEX as method.')

        self.tmp_config['find_periodicities'] = {'method': method, 'method_params': method_params}

        if method == 'FFT':
            # TODO: hier abfangen, dass nicht für mehrdimensionale Daten geht
            per_detect = autocor_4_period.AutoCor4Period(**method_params)
            periodicities, ratings = per_detect.findPeriods(self.train_data)
            self.periodicities = periodicities
            return periodicities, ratings
        if method == 'REGEX':
            per_detect = reg_exp_4_period.RegExp4Period(**method_params)
            return per_detect.find_periods(self.train_data)

    def find_peaks(self, height: float, distance: float) -> [float]:
        """A wrapper function for finding peaks of a certain height and distance in the training data.

        Args:
            height: Minimal height of a peak.
            distance: Minimal distance between two peaks.
        Returns:
            A list containing the identified peaks in the train_data.
        """
        self.tmp_config['find_peaks'] = {'height': height, 'distance': distance}
        self.peaks = periodicity_utils.find_peaks_modified(self.train_data, peak_height=height, peak_distance=distance)
        return self.peaks

    def identify_patterns(self, pattern_length: int, num_patterns: int = None, start_with_zero: bool = False) -> {}:
        """Given an expected pattern length, searches for patterns in the train_data and returns found pattern types.

        Args:
            pattern_length: The expected pattern length.
            num_patterns: Number of different pattern types to cluster, defaults to 5.
            start_with_zero: Boolean defining if a pattern must start with a zero.
        Returns:
            A dictionary containing one pattern per type.
        """
        self.exp_patterns = periodicity_utils.pattern_recommender(pattern_length, self.train_data,
                                                                  num_patterns=num_patterns,
                                                                  start_with_zero=start_with_zero)
        return self.exp_patterns

    def set_pattern(self, pattern: np.array) -> None:
        """Setter method for patterns.

        Args:
            pattern: Pattern to be set for this Segmenter instance.
        """
        self.pattern = pattern

    def slice_pattern(self, start: int, stop: int) -> np.array:
        """This method takes a start and stop index and slices and returns the corresponding train_data slice.

        Args:
            start: Index for beginning of the slice.
            stop: Index for the end of the slice.
        Returns:
            The sliced pattern.
        """
        self.pattern = self.train_data[start:stop]
        return self.pattern

    def find_pattern(self, pattern: np.array = None, prom: float = None, percentile: float = 0.05):
        """Given a pattern, function finds and returns indexes of similar patterns in the data.

        Args:
            pattern: np.array containing one pattern which should be searched.
            prom: Only correlations with a certain prominence are regarded.
            percentile: Only patterns of a similarity of (1 - percentile) * 100 per cent are considered.

        Returns:
            A list of indexes of the locations of the found patterns and a list of similarities.
        """
        self.percentile = percentile
        if pattern is not None:
            self.pattern = pattern

        self.peaks, self.correlations, self.prom = periodicity_utils.find_pattern(np.concatenate(self.pattern, axis=0), np.concatenate(self.train_data, axis=0), step_size=1, percentile=percentile, prom=prom, padding=True)
        return self.peaks, self.correlations

    def mark_in_data(self, mark: str, mark_params: {} = None) -> pd.DataFrame:
        """Function adds periodicity column to data, depending on the method used, which counts up pattern occurrences.

        Args:
            mark: String defining the method, by which occurrences are determined. Can be either periodicities or peaks
            or patterns.
            mark_params: Dictionary containing additional parameters depending on the method used.
        Returns:
            A pandas Dataframe containing the original data and the extra column.
        Raises:
            ValueError: Mark parameters has to be either periodicities, peaks or patterns.
        """
        self.tmp_config['mark_in_data'] = {'mark': mark, 'mark_params': mark_params}

        if mark == 'periodicities':
            if mark_params is None:
                raise ValueError('Dictionary containing key periodicity and corresponding value is needed to call this'
                                 ' method.')
            self.data = periodicity_utils.mark_periodicity(self.data, **mark_params)
        elif mark == 'peaks':
            self.data = periodicity_utils.mark_by_peaks(self.data, self.peaks)
        elif mark == 'patterns':
            if mark_params is None:
                self.prom = None
                self.find_pattern()
                self.tmp_config['mark_in_data']['mark_params'] = {'pattern': self.pattern, 'prom': self.prom,
                                                                  'percentile': self.percentile}
            else:
                self.find_pattern(**mark_params)
                self.tmp_config['mark_in_data']['mark_params'] = {'pattern': self.pattern, 'prom': self.prom,
                                                                  'percentile': self.percentile}
            self.data = periodicity_utils.mark_pattern(self.data, self.peaks, self.pattern)
        else:
            raise ValueError('Mark parameter can either be periodicities, peaks or patterns.')
        return self.data

    def write_to_file(self, file_format: str = 'csv', path: Path = Path.cwd(), folder_name: str = None) -> None:
        """Method is called to write results either as csv or json files in a given folder name.

        Args:
            file_format: Format of the file to be written, either csv or json.
            path: Path to the folder, where a new folder with the found occurrences should be saved.
            folder_name: Name of the new folder, to save the occurrences to.

        Raises:
            ValueError: If data is not processed before calling this method.
            ValueError: If file_format is not either csv or json.
        """
        if 'period' not in self.data.columns.values.tolist():
            raise ValueError('Please call mark in data before attempting to write results to file.')

        if file_format not in ['csv', 'json']:
            raise ValueError('Valid write_to_file formats are csv or json.')

        path = Path(path, str(folder_name))
        if not os.path.exists(path):
            os.makedirs(path)

        patterns_plt = np.sort(self.data['period'].unique())
        for value in patterns_plt:
            if value == -1:
                continue
            else:
                f_name = "occurence_" + str(value)
                current_pattern = self.data[self.data['period'] == value]
                if file_format == 'csv':
                    current_pattern.to_csv(str(path) + "\\" + f_name + ".csv", index=False)
                else:
                    current_pattern.to_json(str(path) + "\\" + f_name + ".json", orient="table", index=False, indent=4)

    def save_to_config(self, name: str = None) -> None:
        """Function to save current pattern to the config file. Use write_config() to save config file to system.

            Args:
                name: String defining the label of the pattern / periodicity which is saved.
        """
        if name is None:
            name = 'pattern_' + str(self.pattern_counter)
        self.pattern_counter += 1
        self.config[name] = copy.deepcopy(self.tmp_config)

    def write_config(self, file_name: str = 'data_segmentation_config.json') -> str:
        """Function formats config in right format and calls write_config() with given name, saving the config to a json
        file.

        Args:
            file_name: File name, where config should be written.

        Returns:
            The filename of the generated json file.
        """
        for key in self.config.keys():
            if self.config[key]['mark_in_data']['mark'] == 'patterns':
                self.config[key]['mark_in_data']['mark_params']['pattern'] = \
                    list(np.concatenate(self.config[key]['mark_in_data']['mark_params']['pattern'], axis=0))
        return super(DataSegmentation, self).write_config(file_name)

    def run(self, run_params: list = None) -> pd.DataFrame:
        """Given a dataset and a config, transforms the data according to the config.

        Args:
            run_params: A list of parameters [data_path, config_path, optional: result_folder_name].
        Returns:
            A pandas Dataframe containing the transformed data.
        """
        super(DataSegmentation, self).run(run_params)

        for key, value in self.config.items():
            self.config = value
            for func, params in self.config.items():
                getattr(self, func)(**params)

        # If a third parameter is given in list and it is set to True: save patterns to individual files
        if run_params is not None:
            if len(run_params) > 2 and run_params[2] == str:
                self.write_to_file(folder_name=run_params[2])

        if self.attrs is not None:
            self.data.attrs = self.attrs
        return self.data


if __name__ == "__main__":
    # params on startup: 1. path to config file, 2. path to data, 3. (optional) folder name to save occurrences to
    segmenter = DataSegmentation()
    segmenter.run(sys.argv[1:])
