import abc
import time
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import OPTICS
from sklearn.preprocessing import StandardScaler
from statsmodels.tsa.holtwinters import ExponentialSmoothing
import pmdarima as pm

from sklearn.decomposition import PCA
import numpy as np


class MLAlgorithmInterface:
    '''
        This is an interface for new ML_Algorithms for industrial machine learning.
    '''
    def __init__(self):
        self._learned = False

    def fit(self, learndata):
        '''
            Method to fit learning data into the algorithm. Triggers learning.
        '''
        self._learned = True

    def predict(self, data):
        '''
            Method to predict outcome from algorithm, given testdata
        '''

    def evaluate(self, testdata):
        '''
            Method to print evaluation of the learning data, given labeled testdata
        '''


class NN(MLAlgorithmInterface, metaclass=abc.ABCMeta):

    def __init__(self, reduction=True, dim_red=3, distance_function="euclidean",
                 anomaly_detection_method="random_plain", min_samples=None):
        '''Initializes the model

            Args:
            reduction           : if dimension reduction is performed (default=True)
            dim_red             : max dimensions the data is reduced to (default = 3)
            dist                : algorithm to use in some functions (default = "euclidean")
            anomaly_detection_method : Different stratagies to set decission boundaries.
                            "random_plain" [default]    This stratagie uses a random
                            "neighbor_support"  With neighbour support
                            "k_neighbor" uses distance of k neighbor from next for threshold
        '''
        # some pre defined configurations
        self._dim = dim_red
        self.dist = distance_function
        self._anomaly_detection_method = anomaly_detection_method
        self.min_samples = min_samples
        self.filter = None
        self.reduction = reduction
        self.dr = None

        # members for fitting
        self.model = None
        self.scaler = StandardScaler()
        self.m_trained = False

        # members for prediction
        self.fc_steps = 1
        self.timediff = 1
        self.sensor_names = False

        # availabele variables depending on modeling function
        #   existance not assured, only when th_random_plain used
        self.covmat = None
        self.invcovmat = None
        self._threshold = None
        self._auto_trains = None

    def fit(self, learndata, autoTestPerc=0.02, preAnalyze=True, data_times=None,
            verbose=False, use_for_prediction=20):
        ''' Train method to learn a model out of given data

            Args:
            learndata:  ndarray for model to be trained on
            autoTestPerc: default=0.02 percent data for training
            preAnalyze: (Optional, default = True, boolean) If True, column-wise filtering of zero variances. Decreases dimensionality of data_matrix
            data_times: (Optional, default = None, ndarray) If given the timestamp of an observation is connected to the prediction and forecast
            verbose:    (Optional, default = True, boolean) If True prints current step
            use_for_prediction: (Optional, default = 20) How many of the learndata -counted from the end- is used for prediction
        '''
        if verbose:
            print('------------------------------------------')
            print('---------- Building New Model ------------')
            print('------------------------------------------')
        self.START = time.clock()

        # Define a filter which data sensors to use. If preanalyze = True use only dimension with standard deviation not equal to zero
        if preAnalyze:
            self.filter = self.__pre_analyse_data(learndata)
        else:
            self.filter = np.ones(learndata.shape[1], dtype=bool)

        # checking if dimension reduction is neccessary
        # if neccessary reduction execute in method random_plain/ neighbor_support for fit
        # and in prediction
        if self._dim > learndata[:, self.filter].shape[1]:
            self._dim = learndata[:, self.filter].shape[1]

        # fit scaler with data with mean_Value=0 and std=1
        # scaling execute in method predict and fit
        self.learndata = learndata
        self.scaler.fit(learndata)

        # fit new data in model dependent on choosen method
        if self._anomaly_detection_method == "random_plain" or self._anomaly_detection_method == "k_neighbor":
            self.__th_random_plain(autoTestPerc)

        elif self._anomaly_detection_method == "neighbor_support":
            self.__th_neighbour_support()

        else:
            print("No valid anomaly detection method!")
            return

        # Build Pandas Dataframe of Predictions of Learndata and its belonging Timestamp
        predictions_list_temp = list()
        data_times_list_temp = list()

        # use only the last x datapoints for first prediction (use_for_prediction default=20)
        for index, values in enumerate(self.learndata[-use_for_prediction:]):
            predictions_list_temp.append(self.predict(values, verbose=False))

            if data_times is not None:
                data_times_list_temp.append(data_times[index])

        # if data_times given, set new timediff (default=1)
        if data_times is not None:
            self.timediff = data_times[1] - data_times[0]
            temp_lists = list(zip(data_times_list_temp, predictions_list_temp))
            self.predictions = pd.DataFrame(temp_lists, columns=['time', 'prediction'])
        else:
            self.predictions = pd.DataFrame()
            self.predictions['prediction'] = predictions_list_temp

        if verbose:
            print("---- fitting takes: {} sec.-----".format(np.round(time.clock() - self.START, 2)))
            print('------------------------------------------\n')

    def predict(self, data, binary=False, sensor_names=None, verbose=False, forecast=False, steps=2,
                fc_used_data_length=1000, fc_type=0, freq=1):
        ''' Interface method anomaly detection methods

            Args:
            Data:           Data samples to be analyzed
            binary:         if the output is binary, default=False
            sensor_names:   A list of names bound to the values wich are given in Data (Optional)
            verbose:        Prints some output
            forecast:       Whether a forecast should be done
            steps:          How many steps in the future are forecasted
            fc_used_data_length:    How many of the past data points are used for the prediction
            fc_type:        Which kind of forecasting is used (0=arima, 1=sarima, 2=exponential smoothing)
            freq:           if using Sarima, how often a seasonal pattern appears in a year (eg. if monthly freq = 12)

            Returns:        probability for an anomaly formatted depending of the dimension of the input
                            Name of sensor closest to the anomaly (Optional)
        '''
        _start_time1 = time.clock()
        if verbose:
            print('------------------------------------------')
            print('--------- Predicting Anomalies -----------')
            print('------------------------------------------')

        # len(data.shape) is 1 by an array and 2 by a matrix
        if sensor_names is not None:
            self.sensors = list(np.asarray(sensor_names)[self.filter])
            if len(data.shape) == 1:
                return self.__predict_anomaly(data, binary=binary, verbose=verbose)
            elif len(data.shape) == 2:
                return self.__predict_anomaly_matrix(data, binary=binary, verbose=verbose)
            else:
                print("Shape of data does not fit model.")
                return None
        else:
            if len(data.shape) == 1:
                self.prediction = self.__predict_anomaly(data, binary=binary, verbose=verbose)
            elif len(data.shape) == 2:
                self.prediction = self.__predict_anomaly_matrix(data, binary=binary, verbose=verbose)

            else:
                print("Shape of data does not fit model.")
                return None

        if verbose:
            print("---- Prediction took: {} sec. ----".format(np.round(time.clock() - _start_time1, 2)))
            print('------------------------------------------\n')

        # optional execute forecast
        if forecast:
            _time_start = time.clock()
            forecast = self.__do_forecast(steps=steps, fc_used_data_length=fc_used_data_length, fc_type=fc_type)
            if verbose:
                print("---- Forecast took: {} sec. ----".format(np.round(time.clock() - _time_start, 2)))
                print('------------------------------------------\n')
            return self.prediction, forecast

        return self.prediction

    def __pre_analyse_data(self, learndata):
        ''' Search for non zero variances in data to filter.
            Args:
            learndata:    data to filter

            Return: filter

        '''
        fil = []
        for i in range(0, learndata.shape[1]):
            stan = np.std(learndata[:, i])
            if stan == 0:
                fil.append(i)
        m_filter = np.ones(learndata.shape[1], dtype=bool)

        m_filter[fil] = False
        return m_filter

    def __fit_NN(self):
        ''' fit model
            metric: choosen distant function
            model_points: transform data from method perform_dr

            self.nbrs for later use

        '''
        if self.dist == "mahalanobis":
            self.nbrs = NearestNeighbors(algorithm='brute', metric=self.dist, metric_params={'VI': self.invcovmat})
        else:
            self.nbrs = NearestNeighbors(algorithm='kd_tree', metric=self.dist)

        self.nbrs.fit(self.model_points)
        self.m_trained = True

    def __th_random_plain(self, perc):
        ''' method random plain, use max or median+ 2x std as threshold for anomaly
            reduce dimension of lerndata to choosen dim

            Args:
            perc: percent of data for testing

        '''
        extract = int(self.learndata.shape[0] * perc)
        auto_ix = np.random.choice(range(self.learndata.shape[0]), size=extract, replace=False)

        self.auto_test_data = self.learndata[auto_ix]
        self.learndata = np.delete(self.learndata, auto_ix, axis=0)

        self.learndata = self.scaler.transform(self.learndata)

        # dimension reduction and setting this as modelpoints
        if self.reduction == True:
            self.perform_dr(self.learndata[:, self.filter])
        else:
            self.model_points = self.learndata[:, self.filter]

        if self.dist == "mahalanobis":
            self.covmat = np.cov(np.transpose(self.model_points))
            self.invcovmat = np.linalg.inv(self.covmat)

        self.__fit_NN()

        self._auto_trains = self.predict(self.auto_test_data)

        # TODO threshold as mean from a different amount of neighbour
        # self._threshold = np.median(self._auto_trains) + np.std(self._auto_trains) # 3xstd?
        self._threshold = np.max(self._auto_trains) + 2 * np.std(self._auto_trains)

    def __th_neighbour_support(self):
        '''method neighbour_support
           reduce dimension of lerndata to choosen dim
           fit model with dbscan

        '''
        # TODO: Auto fit modeldata, get support for each, calc support stats ... do magic

        if self.min_samples is None:
            self.min_samples = int(self.learndata.shape[0] * 0.01)
            if self.min_samples < 1:
                self.min_samples = 1

        # dimension reduction and setting this as modelpoints
        if self.reduction == True:
            self.perform_dr(self.learndata[:, self.filter])
        else:
            self.model_points = self.learndata[:, self.filter]

        self.model = OPTICS(eps=3.1, min_samples=self.min_samples, cluster_method="dbscan", algorithm="kd_tree").fit(
            self.model_points)
        print("Number of clusters: ", len(set(self.model.labels_)))
        print("Number of noisy data: ", list(self.model.labels_).count(-1))

        self.__fit_NN()

        return None

    def __do_forecast(self, steps=2, fc_used_data_length=1000, fc_type=0, freq=1):
        '''Forecasts the model using arima, sarima or exponential smoothing.

            Args:
            steps:          how many steps in the future are forecasted
            fc_used_data_length:   number of data points used for the arima model, if too high the algorithm needs much more time
            fc_type:        which model ist used to forecast (0 = arima, 1 = sarima, 2 = exponential smoothing)
            freq:           if using Sarima, how often a seasonal pattern appears in a year (eg. if monthly freq = 12)
            Returns:
                 probability if the next states are valid or not, as an pandas DataFrame
        '''
        self.fc_steps = steps

        # TODO: forecast with data and then prediction in forecast
        # check if all predictions are the same. Usually this can happen in binary = True (Arg of prediction)
        if len(np.unique(self.predictions.to_numpy())) > 1:
            if fc_type == 0:
                return self.__arima(self.predictions.to_numpy(), fc_used_data_length=fc_used_data_length)
            elif fc_type == 1:
                return self.__sarima(self.predictions.to_numpy(), fc_used_data_length=fc_used_data_length, freq=freq)
            elif fc_type == 2:
                return self.__exponential_smoothing(self.predictions.to_numpy(),
                                                    fc_used_data_length=fc_used_data_length)
            else:
                print("No valid forecast model chosen!")
        # If all prediction entries are the same, then prediction will not work.
        # So the output is also the same as prediction. At least the function will work.
        else:
            print("\n All internal prediction states are the same! So forcast will also be the same.")
            return self.predictions.to_numpy()[0:steps]

    def __predict_anomaly(self, data_vector, binary, verbose=False):
        ''' Giving a new data sample, the model will decide whether this is an anomaly or not

            Args:
            data_vector:    ndarray containing all sensors used for the model.
            orig_dist:      (optional, default = False, boolean) If True output is original Euclidean distance without using Marr wavelet.

            Returns:
                            probability for an anomaly: boolean if binary=True
                                                        float, if method="neighbor_support" and binary=false

        '''

        # filter sensor values and transform data_vector to choosen dimension space
        data_vector = data_vector[self.filter]
        red_vec = self.scaler.transform(data_vector.reshape(1, -1))
        if self.reduction == True:
            red_vec = self.transform_data(red_vec)

        # 1.) find nearest data point
        if self._anomaly_detection_method == "random_plain":
            # get distance and index from nearest neighbour
            dist, index = self.nbrs.kneighbors(red_vec, 1, return_distance=True)
            out = dist[0][0]  # TODO: This unpacking is not nice!!! output so woud be (array([[0.5]])

            if binary:  # binary in fitting is always False to set threshold
                if dist >= self._threshold:
                    out = 1
                elif dist <= self._threshold:
                    out = 0

            return out

        # 2.) distance k neighbor
        if self._anomaly_detection_method == "k_neighbor":

            dist, index = self.nbrs.kneighbors(red_vec, 1, return_distance=True)
            index = index.reshape(-1, 1)
            dist = dist.reshape(-1, 1)
            new_data = self.model_points[index[0]]

            k = 6  # number of neighbors
            k_dist, index = self.nbrs.kneighbors(new_data, k, return_distance=True)

            MW = np.mean(k_dist)
            std = np.std(k_dist)

            if binary:
                if dist > MW + 3 * std:
                    out = 1
                else:
                    out = 0
            else:
                out = dist / MW

            return out

        # 3.) with DBSCAN
        elif self._anomaly_detection_method == "neighbor_support":
            # get distance and index from nearest neighbour
            dist, index = self.nbrs.kneighbors(red_vec, 1, return_distance=True)
            dist = dist.reshape(-1, 1)
            index = index.reshape(-1, 1)
            d = dist
            # get core_dist and reachability from dbscan model for nearest neighbour
            core_dist = self.model.core_distances_[self.model.ordering_][index]
            reachab = self.model.reachability_[self.model.ordering_][index]

            _is_cluster = self.model.labels_[index] >= 0  # noise=-1
            _is_in_coreDistToNN = (core_dist - d) >= 0
            if reachab != np.inf:
                _is_reachable = (reachab - d) >= 0
            else:  # reachable=np.inf if never be corepoint
                _is_reachable = False

            truth = bool(_is_cluster and _is_in_coreDistToNN and _is_reachable)

            if binary:
                if truth:
                    out = 0
                else:
                    out = 1
            else:
                out = float(d / core_dist)  # >1 anomaly
            return out

    def __predict_anomaly_matrix(self, data_matrix, binary, verbose=False):
        ''' Giving a new data matrix, the model will decide whether this is an anomaly or not

            Args:
            data_matrix:    2d_sndarray containing all sensors used for the model

            Returns:
                            array of probabilities for an anomaly
        '''
        sol = []

        for i in range(data_matrix.shape[0]):
            obs = data_matrix[i, :]
            sol.append(self.__predict_anomaly(obs, binary=binary))
        return np.asarray(sol)

    def __exponential_smoothing(self, data, fc_used_data_length):
        ''' A forecast from the given data is created using the exponential_smoothing model. To get the model
            with the best fitting parameters exponential_smoothing uses an optimisation,
            but those can be overridden when needed.

            Args:
            data:           data on which the model ist build
            fc_used_data_length:    how many of the steps in the past are used for the model

            Returns:        forecast whether the next states -how many depend on self.fc_steps- are valid

        '''

        # data.T[1] data as ndarray daher mit T und [1] zugriff auf alle Daten in 'spalte' 1
        if 'time' in self.predictions:
            if len(data.T[1]) > fc_used_data_length:
                datum = data.T[1][-fc_used_data_length:]
            else:
                datum = data.T[1]
        else:
            if len(data.T[0]) > fc_used_data_length:
                datum = data.T[0][-fc_used_data_length:]
            else:
                datum = data.T[0]

        model = ExponentialSmoothing(datum)  # Holt Winter’s Exponential Smoothing
        model_fit = model.fit()
        forecast_values = model_fit.predict(start=0, end=self.fc_steps - 1)

        # Build time for forecasted data points and connect it with the corresponding data points
        # only valid for equidistant time series
        # if 'time' in self.predictions['time'].empty:
        if 'time' in self.predictions:
            forecast_times = list()
            for t in range(self.fc_steps):
                forecast_times.append(self.predictions['time'].iloc[-1] + self.timediff * (1 + t))

            temp_list = list(zip(forecast_times, forecast_values))

            return pd.DataFrame(temp_list, columns=('time', 'forecast'))
        else:
            forecast = pd.DataFrame()
            forecast['forecast'] = forecast_values
            return forecast

    def __arima(self, data, fc_used_data_length):
        ''' A forecast from the given data is created using the arima model. To get the model with the best fitting
            parameters arima is used via the auto_arima function.

            Args:
            data:                   data on which the model ist build (optional with timestamp)
            fc_used_data_length:    how many of the steps in the past are used for the model

            Returns:        forecast whether the next states -how many depend on self.fc_steps- are valid as a pandas
                                DataFrame, if a timestamp is provided a fitting timestamp is attached

        '''

        if 'time' in self.predictions:
            if len(data.T[1]) > fc_used_data_length:
                datum = data.T[1][-fc_used_data_length:]
            else:
                datum = data.T[1]
        else:
            if len(data.T[0]) > fc_used_data_length:
                datum = data.T[0][-fc_used_data_length:]
            else:
                datum = data.T[0]

        # possible parameters:
        # https://www.alkaline-ml.com/pmdarima/modules/generated/pmdarima.arima.auto_arima.html#pmdarima.arima.auto_arima
        # tips to use arima in general: https://www.alkaline-ml.com/pmdarima/tips_and_tricks.html#tips-and-tricks
        model = pm.auto_arima(datum, start_p=1, start_q=1,
                              test='adf',  # use adftest to find optimal 'd'
                              max_p=3, max_q=3,  # maximum p and q
                              m=1,  # frequency of series; m=1 forced seasonal=false!
                              d=None,  # let model determine 'd'
                              seasonal=False,
                              error_action='ignore',  # don't want to know if an order does not work
                              suppress_warnings=True,
                              stepwise=True)

        forecast_values = model.predict(self.fc_steps)

        # Build time for forecasted data points and connect it with the corresponding data points
        # only valid for equidistant time series
        if 'time' in self.predictions:

            forecast_times = list()
            for t in range(self.fc_steps):
                forecast_times.append(self.predictions['time'].iloc[-1] + self.timediff * (1 + t))

            temp_list = list(zip(forecast_times, forecast_values))

            return pd.DataFrame(temp_list, columns=('time', 'forecast'))
        else:
            forecast = pd.DataFrame()
            forecast['forecast'] = forecast_values
            return forecast

    def __sarima(self, data, fc_used_data_length, freq):

        ''' A forecast from the given data is created using the sarima model. To get the model with the best fitting
            parameters sarima is used via the auto_arima function.

            Args:
            data:                   data on which the model ist build (optional with timestamp)
            fc_used_data_length:    how many of the steps in the past are used for the model
            freq:           if using Sarima, how often a seasonal pattern appears in a year (eg. if monthly freq = 12)

            Returns:        forecast whether the next states -how many depend on self.fc_steps- are valid as a pandas
                                DataFrame, if a timestamp is provided a fitting timestamp ist attached
        '''

        if 'time' in self.predictions:
            if len(data.T[1]) > fc_used_data_length:
                datum = data.T[1][-fc_used_data_length:]
            else:
                datum = data.T[1]
        else:
            if len(data.T[0]) > fc_used_data_length:
                datum = data.T[0][-fc_used_data_length:]
            else:
                datum = data.T[0]

        # possible parameters:
        # https://www.alkaline-ml.com/pmdarima/modules/generated/pmdarima.arima.auto_arima.html#pmdarima.arima.auto_arima
        # tips to use arima in general: https://www.alkaline-ml.com/pmdarima/tips_and_tricks.html#tips-and-tricks
        model = pm.auto_arima(datum, start_p=1, start_q=1,
                              test='adf',  # use adftest to find optimal 'd'
                              max_p=3, max_q=3,  # maximum p and q
                              m=freq,  # frequency of series zb 7-daily 12-monthly
                              d=None,  # if none let model determine 'd'
                              seasonal=True,  # Seasonality
                              start_P=0,  # order seasonal component
                              D=1,  # integration seasonal component
                              error_action='ignore',
                              suppress_warnings=True,
                              stepwise=True)

        forecast_values = model.predict(self.fc_steps)

        # Build time for forecasted data points and connect it with the corresponding data points
        # only valid for equidistant time series
        if 'time' in self.predictions:

            forecast_times = list()
            for t in range(self.fc_steps):
                forecast_times.append(self.predictions['time'].iloc[-1] + self.timediff * (1 + t))

            temp_list = list(zip(forecast_times, forecast_values))

            return pd.DataFrame(temp_list, columns=('time', 'forecast'))
        else:
            forecast = pd.DataFrame()
            forecast['forecast'] = forecast_values
            return forecast

    @abc.abstractmethod
    def transform_data(self, data_vector):
        ''' Transforms the given date to the required form

            Args:
            data_vector:    ndarray of data which shall be transformed

            Returns:
                            ndarray of transformed data
        '''

    @abc.abstractmethod
    def perform_dr(self, data_matrix, dim=3):
        ''' Reduces the dimensions of the given data matrix to the chosen ones.

            Args:
            data_matrix:    ndarray of data which shall be reduced
            dim:            dimension of the array to be kept

        '''

    @abc.abstractmethod
    def find_influences(self, model_vec, sample, absolute=True):
        '''
        Function to determine the most influential signals of the current vector.

        Input
            sensors: a list of names connected to the signals rows in data vector
            absolute: (boolean)
        Return
            returns the most influential signals of the current data vector - does it??
        '''


class LDR(NN):
    ''' Class in which the used dimension reduction algorithm is implemented.
        In this case PCA is used.
        If an other algorithm is wanted the methods have to be adjusted by keeping the interface.
        They are used in the inherited methods where the Nearest Neighbour algorithm is implemented.
    '''

    def transform_data(self, data_vector):
        ''' Transforms the given date to the required form

            Args:
            data_vector:    ndarray of data which shall be transformed

            Returns:
                            ndarray of transformed data
        '''
        return self.dr.transform(data_vector.reshape(1, -1)).reshape(1, self._dim)

    def perform_dr(self, data_matrix):
        ''' Reduces the dimensions of the given data matrix to the chosen ones.

            Args:
            data_matrix:    ndarray of data which shall be reduced
            dim:            dimension of the array to be kept
        '''
        self.dr = PCA(n_components=self._dim)
        self.dr.fit(data_matrix)
        self.model_points = self.dr.transform(data_matrix)

    def find_influences(self, model_vec, sample, absolute=True): #TODO not tested
        '''
        Function to determine the most influential signals of the current vector.

        Input
            sensors: a list of names connected to the signals rows in data vector
            absolute: (boolean)
        Return
            returns the most influential signals of the current data vector - does it??
        '''
        sample_norm = self.dr.inverse_transform(sample)
        model_norm = self.dr.inverse_transform(model_vec)
        if absolute:
            dist = abs((sample_norm-model_norm)[0][0])
        else:
            dist = (sample_norm-model_norm)[0][0]
        joint = np.column_stack(([dist, self.sensors]))
        dist_sort = joint[np.argsort(joint[:, 0])]
        return dist_sort[::-1]

    def set_dim(self, d):
        self._dim = d

