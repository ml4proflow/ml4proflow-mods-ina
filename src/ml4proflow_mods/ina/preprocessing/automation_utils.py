class Node:
    """Class representing all information stored in one DAF node.
    """
    def __init__(self, name: str, children: [] = None, parents: [] = None, generation_method: str = None,
                 generation_method_params: {} = None, out_node: bool = None) -> None:
        """Generates new node by information given, must at least have a name.
        """
        # list of parents must all be used to generate child via the same method (in one go)
        self.name = name
        self.children = []
        if children is not None:
            self.children = children
        self.parents = []
        if parents is not None:
            self.parents = parents
        self.generation_method = generation_method
        self.generation_method_params = generation_method_params
        self.data = None
        self.out_node = False
        if out_node is not None:
            self.out_node = out_node

    def to_parsable(self) -> {}:
        """Converts all information stored in this node to a dictionary and returns it.

        Returns:
            A dictionary containing the name, children, parents, generation_method, generation_method_params and whether
            this is an outnode.
        """
        return {'name': self.name, 'children': [children.name for children in self.children],
                'parents': [parent.name for parent in self.parents],
                'generation_method': self.generation_method, 'generation_method_params': self.generation_method_params,
                'out_node': self.out_node}

    def set_method(self, method: str, parameter: {}) -> None:
        """Setter method for node generation_method and generation_method_params.

        Args:
            method: String representing the generation_method of this node.
            parameter: Dictionary defining additional parameters used by the generation method.
        """
        self.generation_method = method
        self.generation_method_params = parameter

    def add_parent(self, parent) -> None:
        """Adds a given parent node to the list of parents.

        Args:
            parent: Node which is added to list of parents of this node.
        """
        self.parents.append(parent)

    def add_child(self, child) -> None:
        """Adds a given child node to the list of children.

        Args:
            child: Node which is added to the list of children of this node.
        """
        self.children.append(child)

    def to_outnode(self) -> None:
        """Sets this node as outnode by setting self.out_node to True.
        """
        self.out_node = True

    def remove_outnode(self) -> None:
        """Sets this node as not being an outnode by setting self.out_node to False.
        """
        self.out_node = False


class DAG:
    """Directed acyclic graph to handle automatic preprocessing through sequentially stepping through nodes in the
    graph.
    """
    def __init__(self, config: {} = None) -> None:
        """If a config dictionary is given, it is used to build the DAG. Otherwise, a new empty DAG ist initialized.

        Args:
            config: Dictionary containing all information to built a DAG from.
        """
        self.out_nodes = []
        self.nodes = {}
        if config is not None:
            for key, value in config.items():
                self.nodes[key] = Node(**value)
            # Change names of parents and children back to references to node objects
            for node in self.nodes.values():
                if node.out_node:
                    self.out_nodes.append(node)
                children_ref = [self.nodes[child] for child in node.children]
                parents_ref = [self.nodes[parent] for parent in node.parents]
                node.children = children_ref.copy()
                node.parents = parents_ref.copy()

    def _add_node(self, node_name: str) -> Node:
        """A new node ist created and added to the DAG.

        Args:
            node_name: String representing the name of the new node.
        """
        self.nodes[node_name] = Node(node_name)
        return self.nodes[node_name]

    def add_method(self, applied_on: list, method: str, params: {}, result_data_name: str) -> None:
        """Given a preprocessing method applied on data: adds at least a node representing the resulting data as
        outnode to the DAG.

        Args:
            applied_on: A list of columns on which the method is applied.
            method: A string defining the preprocessing method used.
            params: A dict defining additional parameters set for the method.
            result_data_name: The set name of the resulting data column.
        """
        res_node = self._add_node(result_data_name)
        res_node.set_method(method, params)
        res_node.to_outnode()
        self.out_nodes.append(res_node)

        for data_in in applied_on:
            if data_in not in self.nodes:
                self._add_node(data_in)
            self.nodes[data_in].add_child(res_node)
            res_node.add_parent(self.nodes[data_in])

    def get_dict(self) -> {}:
        """Method parses the DAG nodes to a dictionary which can be saved as e.g. json.
        """
        ret_dict = dict()
        for key, value in self.nodes.items():
            ret_dict[key] = value.to_parsable()
        return ret_dict
