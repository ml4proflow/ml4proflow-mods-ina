from __future__ import print_function, annotations
import pandas

from ml4proflow.modules import Module, DataFlowManager, SourceModule
from . import preprocessing


class PreprocessingModule(Module):
    def __init__(self, dfm: DataFlowManager, config):
        Module.__init__(self, dfm, config)
        self.preprocessor = preprocessing.Preprocessing()
        self.config = config
        self.config.setdefault('mode', 'Online')
        self.config.setdefault('process_config', 'config.json')
        self.config_range['mode'] = ['Training', 'Online']

    def on_new_data(self, name: str, sender: SourceModule, data: pandas.DataFrame):
        if self.config['mode'] == 'Online':
            if (self.config['process_config'] is None) or (len(data) == 0):
                return
            self.preprocessor.set_data(dataframe=data)
            self.preprocessor.set_config(self.config['process_config'])
            result = self.preprocessor.run()
            for channel in self.config['channels_push']:
                self._push_data(channel, result)
        elif self.config['mode'] == 'Training':
            self.preprocessor.set_data(dataframe=data)
        else:
            raise ValueError('Mode must be run or configuration.')

    @classmethod
    def get_module_desc(cls):
        return {"name": "Preprocessing",
                "categories": ["Preprocessing"],
                "jupyter-gui-cls": "ml4proflow_mods.ina.widgets.preprocessing_widget.PreprocessingWidget",
                "jupyter-gui-override-settings-type": {'process_config': 'file'},
                "html-description": """
                <H1 style="margin-bottom:6px;margin-top:6px">Preprocessing</H1>
                <h2 style="margin-bottom:1pt">Description</h2><p>This process handles basic preprocessing, such as choosing columns, scaling, filtering and dimensionality reduction.</p><h2 style="margin-bottom:1pt">Usage</h2><p>As preprocessing step for further processing by other modules.</p>
                """
                }
