import pandas as pd
import json
import sys
from enum import Enum
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler

from .dimension_reduction_pca import LDR
from .automation_utils import DAG
from .base_process import BaseProcess
from . import preprocessing_utils


class Method(str, Enum):
    SCALE = 'Scale'
    FILTER = 'Filter'
    REDUCE_DIMENSION = 'Reduce Dimension'
    NOTHING = 'Nothing'
    DROP_SIGNAL = 'Drop Signal'


class Preprocessing(BaseProcess):
    """A class to configure and run a preprocessing on a given dataset.

    This class implements the functions given in toolbox.preprocessing to use on a given dataset.
    As a toolbox process, the class inherits from the BaseProcess class in process.py.

    Attributes:
        data: A pandas dataframe containing the un-preprocessed data, initialised by base class.
        methods: A list of strings concerning to the preprocessing methods implemented by this class.
        temporary_out: A pandas dataframe which temporarily stores the result of an applied preprocessing method.
        preprocessed_data: A pandas dataframe storing the final preprocessing output.
        config: A DAG representing all preprocessing decisions made to store them in a config.
        tmp_node: A helper temporarily storing current config parameters.
    """
    def __init__(self, dataframe: pd.DataFrame = None, config: {} or str = None):
        """Inits Preprocessing using a pandas dataframe as raw data if given."""
        super().__init__(dataframe)
        if config is not None:
            self.set_config(config=config)
        else:
            self.config = DAG()
        self.methods = [method.value for method in Method]
        self.temporary_out = self.data
        self.preprocessed_data = pd.DataFrame()
        self.tmp_node = None

    def get_methods(self):
        return self.methods

    def apply_method(self, source: str, columns: [], method: str, params=None) -> pd.DataFrame:
        """Applies a given method on data defined by source and column.

        Args:
            source: String, which can either be Raw data to use the unprocessed data or Preprocessed data to use
            previously processed data to apply the method on.
            columns: List of column names the method should be applied on.
            method: String representing the name of the method to use. Can be Drop Signal, Filter, Scale, Reduce
            Dimension or Nothing.
            params: Additional params relevant to the selected method.

        Returns:
            A pandas Dataframe containing the processed data.
        """
        self.tmp_node = {'applied_on': columns, 'method': method, 'params': params}

        # 1. Slice correct data given by source and columns
        if source == 'Raw data':
            column_idx = []
            for element in columns:
                column_idx.append(list(self.data.columns.values).index(element))
            tmp_data = self.data.to_numpy()[:, column_idx]

        else:
            if self.preprocessed_data.empty:
                raise ValueError('Cannot use method on preprocessed data, because it is empty')
            column_idx = []
            for element in columns:
                column_idx.append(list(self.preprocessed_data.columns.values).index(element))
            tmp_data = self.preprocessed_data.to_numpy()[:, column_idx]

        if method == 'Drop Signal':
            if source == 'Preprocessed data':
                self.preprocessed_data.drop(columns, axis='columns', inplace=True)
                for column in columns:
                    self.config.nodes[column].remove_outnode()
            self.temporary_out = self.preprocessed_data
            return self.temporary_out

        self.method_wrapper(tmp_data, method, columns, params)

        return self.temporary_out

    def method_wrapper(self, data: np.ndarray, method: str, column_names: [] = None, params=None) -> pd.DataFrame:
        """Method wrapper called from within apply_method() with the selected data given as np.ndarray.

        Args:
            data: Data on which method should be applied.
            method: String representing method: Nothing, Filter, Scale or Reduce Dimension
            column_names: List of column names corresponding to data, used to generate the output dataframe.
            params: Additional parameters corresponding to the chosen method.

        Returns:
            A pandas Dataframe containing the processed data.
        """
        if self.tmp_node is None:
            self.tmp_node = {'applied_on': column_names, 'method': method, 'params': params}

        if method == 'Nothing':
            self.temporary_out = pd.DataFrame(data=data, columns=column_names)
            self.tmp_node['applied_on'] = []

        if method == 'Filter':
            if params is None:
                params = 'lowpass'
                self.tmp_node['params'] = params
            self.temporary_out = pd.DataFrame(data=preprocessing_utils.filter(data, params),
                                              columns=[str(name) + '_filtered' for name in column_names])

        if method == 'Scale':
            scaler = StandardScaler(with_std=False)
            data = scaler.fit_transform(data)
            scaler = MinMaxScaler()
            self.temporary_out = pd.DataFrame(scaler.fit_transform(data),
                                              columns=[str(name) + '_scaled' for name in column_names])

        if method == 'Reduce Dimension':
            reducer = LDR()
            reducer.set_dim(1)
            reducer.perform_dr(data)
            self.temporary_out = pd.DataFrame(np.array(list(reducer.model_points)),
                                              columns=[str(column_names[0]) + '_dimred'])

        return self.temporary_out

    def add_to_output(self, column_name: str = None) -> None:
        """Add the last applied method and its params to the config.
        """
        self.tmp_node['result_data_name'] = column_name
        # TODO: check if Node already exists if no column name is given
        if column_name in self.config.nodes.keys():
            self.config.nodes[column_name].to_outnode()
        else:
            self.config.add_method(**self.tmp_node)
        self.tmp_node = None

        # if column name given, rename correspondingly (if number of columns in temporary out == 1)
        if column_name is not None:
            if self.temporary_out.shape[1] == 1:
                self.temporary_out.rename(columns={self.temporary_out.columns[0]: column_name}, inplace=True)
        if self.preprocessed_data.empty:
            self.preprocessed_data = self.temporary_out
        else:
            self.preprocessed_data = self.preprocessed_data.join(self.temporary_out)

    def set_config(self, config: {} or str = None) -> None:
        """Calls base class to read config dictionary and converts it into a DAG for this process.

        Args:
            path: A path as a string to a .json config file.
            config: A dictionary read at another location representing the config for this process.
        """
        super(Preprocessing, self).set_config(config)
        self.config = DAG(self.config)

    def write_config(self, file_name: str = 'preprocessing_config.json') -> str:
        """Saves generated config as json under given file_name.
        
        Function overrides base class function, as a DAG is used as config here instead of a regular dictionary.
        
        Args:
            file_name: Name of json file where config is saved to.

        Returns:
            The filename of the generated json file.
        """
        with open(file_name, 'w') as f:
            json.dump(self.config.get_dict(), f, indent=4)
        return file_name

    def _calculate_node(self, node) -> None:
        """Given a node from the config, calculate it by recursively calculating the parents.

        Args:
            node: A DAG node object from the config, which is to be calculated.
        """
        if node.data is not None:
            return
        if len(node.parents) == 0:
            # If node is start node (has no parents): data has to be allocated from raw data (self.data)
            node.data = np.array([self.data[node.name]]).T
            return
        # Else: if node has parents: calculate node by recursively calculating parents
        for parent_node in node.parents:
            self._calculate_node(parent_node)
        if len(node.parents) > 1:
            data = np.column_stack([parent_node.data for parent_node in node.parents])
            column_names = [parent_node.name for parent_node in node.parents]
        else:
            data = node.parents[0].data
            column_names = [node.parents[0].name]
        # TODO: add generation_method params
        node.data = self.method_wrapper(data, node.generation_method, column_names)

    def run(self, run_params: list = None) -> pd.DataFrame:
        """Given data and a config starts running preprocessing automatically.

        Data and config can either be set in separate function or can be given as parameters in the run_params list,
        this is done by base class.
        If a True is given as a third run parameter in the list, the resulting preprocessed data is saved to a .csv
        called preprocessed_data.csv.

        Args:
            run_params: Is a list consisting of two paths (strings):
            [path to config file (.json), path to data file (.csv)]

        Returns:
            The preprocessed data in a pandas dataframe.
        """
        super(Preprocessing, self).run(run_params)

        for idx, out_node in enumerate(self.config.out_nodes):
            self._calculate_node(out_node)

        self.preprocessed_data = pd.DataFrame(np.column_stack([out_node.data for out_node in self.config.out_nodes]),
                                              columns=[out_node.name for out_node in self.config.out_nodes])

        # If a third parameter is given in list and it is set to True: save data as .csv
        if (run_params is not None) and len(run_params) > 2:
            if run_params[2]:
                self.preprocessed_data.to_csv('preprocessed_data.csv', index=False, sep=';')

        return self.preprocessed_data


if __name__ == "__main__":
    # Params on startup: 1. path to config file, 2. path to data file, 3. True/False whether result should be saved as csv
    preprocessor = Preprocessing()
    preprocessor.run(sys.argv[1:])




