from __future__ import annotations
import pandas as pd
import json
from typing import Any
from ml4proflow.modules import Module, DataFlowManager


class BaseProcess:
    """This class implements functions which are needed by toolbox processes.

    Example usage to run a process (Process inherits from BaseProcess) automatically:

    processed_data = Process(data: pd.Dataframe, config: {}).run()

    ---

    p = Process()

    p.set_data(path: str)

    p.set_config(path: str)

    processed_data = p.run()

    ---

    p = Processor()

    processed_data = p.run([path_config: str, path_data:str])

    Attributes:
        data: A pandas dataframe containing unprocessed data, either initialised or set using set_data().
        attrs: A dictionary containing meta data concerning to the dataframe.
        config: By default a dictionary storing the process configuration for automated execution.
    """
    def __init__(self, dataframe: pd.DataFrame = None, config: {} = None):
        """Inits Preprocessing using a pandas dataframe as raw data if given."""
        self.data = dataframe
        self.attrs = None
        if self.data is not None:
            if self.data.attrs is not None:
                self.attrs = self.data.attrs
        self.config = {}
        if config is not None:
            self.config = config

    def set_config(self, config: {} or str) -> None:
        """Sets config dictionary by reading a json from a given path or by using a given config dictionary.

        Args:
            config: A dictionary functioning as a config for this process or a path as a string to a .json config file.
        """
        if type(config) == str:
            with open(config, 'r') as f:
                self.config = json.load(f)
            return
        self.config = config

    def set_data(self, dataframe: pd.DataFrame = None, path: str = None) -> None:
        """Sets unprocessed data from either a pandas dataframe or a given path to a csv separated by ';'.

        Args:
            dataframe: A pandas dataframe containing unprocessed data.
            path: A string, which is a path to a csv, separated by ';' from which the data should be read.

        Raises:
            ValueError: If neither dataframe nor path is given as a parameter.
        """
        if dataframe is not None:
            self.data = dataframe
            if self.data.attrs is not None:
                self.attrs = self.data.attrs
        elif path is not None:
            self.data = pd.read_csv(path, sep=';')
            self.data.attrs['source_name'] = path
            self.attrs = self.data.attrs
        else:
            raise ValueError('Neither dataframe nor path is given')

    def write_config(self, file_name: str = 'config.json') -> str:
        """Saves generated config as json under given file_name.

        Args:
            file_name: Name of json file where config is saved to.

        Returns:
            The filename of the generated json file.
        """
        with open(file_name, 'w') as f:
            json.dump(self.config, f, indent=4)
        return file_name

    def run(self, run_params: list = None):
        """Given data and a config starts running process automatically as configured on data.

        Data and config can either be set in separate function or can be given as parameters in the run_params list.

        Args:
            run_params: Is a list consisting of two paths (strings):
            [path to config file (.json), path to data file (.csv)]

        Returns:
            The processed data in a pandas dataframe. Has to be implemented by subclass.
        """
        if run_params is not None:
            self.set_config(run_params[0])
            self.set_data(path=run_params[1])

