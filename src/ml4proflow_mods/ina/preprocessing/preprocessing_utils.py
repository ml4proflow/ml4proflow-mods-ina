import numpy as np
from scipy.signal import butter, sosfilt


def filter(signal, filter):
    def apply_sosfilt(signal, filter):
        return sosfilt(filter, signal)
    filter = butter(2, 0.15, filter, output='sos')
    return np.apply_along_axis(apply_sosfilt, 0, signal, filter)
