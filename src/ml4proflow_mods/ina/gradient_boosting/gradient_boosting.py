# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 13:25:12 2021

@author: bucfra

gradient boosting classifier for use case schweissen weidmueller
"""
import sys
import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier

from .base_process import BaseProcess


class GradientBoosting(BaseProcess):
    """This class handles all functions necessary to apply the Method Gradient Boosting to data.

    Attributes:
        data: Pandas dataframe containing the process data which is to be analyzed
        config: Python dictionary containing all parameters to reapply inference on a trained model
        X_train: Pandas dataframe set in read_and_split_data().
        X_test: Pandas dataframe set in read_and_split_data().
        y_train: Label array set in read_and_split_data().
        y_test: Label array set in read_and_split_data().
        pred_label_list: predicted labels for self.data, set in infer_anomaly().

    """

    def __init__(self, dataframe: pd.DataFrame = None, config: {} = None):
        super().__init__(dataframe, config)
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.pred_label_list = None

    def read_and_split_data(self, split=True) -> pd.DataFrame:
        """This function takes the dataframe from self.data with the labels from the metadata and segments it along the period column.

        If the parameter split is set to true, the segmented data is divided into a train and a test validation set.

        Args:
            split: Boolean value specifying if returned dataframe is splitted into training and test data

        Raises:
            ValueError: If no data is set or data is not segmented (has no column period)

        Returns:
            Returns a pandas dataframe and a label array in the right format to fit a model to.
            If split==True, two dataframes and two label arrays, one for
            training and one for validation, are returned.
        """
        if self.data is None or 'period' not in self.data.columns:
            raise ValueError("Please make sure a segmented dataframe can be used for training")

        final_dataframe = pd.DataFrame()
        unique_patterns = np.sort(self.data['period'].unique())
        labels = self.data.attrs['label'] # TODO label aus metadata!

        for batch_number, pattern_number in enumerate(unique_patterns):
            if pattern_number == -1:
                continue
            tmp_data = self.data.loc[self.data['period'] == pattern_number].copy()
            tmp_data.drop(['period'], axis='columns', inplace=True)
            final_dataframe[str(batch_number)] = tmp_data.reset_index(drop=True)

        final_dataframe = final_dataframe.transpose()

        if split:
            return train_test_split(final_dataframe, labels, test_size=0.2)

        return final_dataframe, labels

    def fit_model(self):
        """Takes data, splits it into train and test, saves the model parameters to config and returns model performance
        measures on the testdata.

        Returns:
            Accuracy and Balance Accuracy as quality parameters of the model
            with the testdata.
        """
        self.X_train, self.X_test, self.y_train, self.y_test = self.read_and_split_data()
        self.model = GradientBoostingClassifier()
        self.config['model'] = self.model.fit(self.X_train, self.y_train)
        y_pred = self.model.predict(self.X_test)
        acc = metrics.accuracy_score(self.y_test, y_pred)
        bal_acc = metrics.balanced_accuracy_score(self.y_test, y_pred)
        return acc, bal_acc

    def infer_anomaly(self):
        """This function calculates if the current self.data has anomalies.

        Returns:
            A list of all predicted labels.
        """
        self.traindata = self.read_and_split_data(split=False)
        self.pred_label_list = []

        for i in range(0, len(self.traindata)):
            prediction = self.model.predict(self.traindata.iloc[i])
            self.pred_label_list.append(prediction)
        return self.pred_label_list

    def write_config(self, file_name: str = 'GBoost_analysis.json') -> str:
        self.config['model'] = self.model
        return super(GradientBoosting, self).write_config(file_name)

    def run(self, run_params: list = None):
        super(GradientBoosting, self).run(run_params)
        return self.infer_anomaly()


if __name__ == '__main__':
    analyzer = GradientBoosting()
    analyzer.run(sys.argv[1:])
