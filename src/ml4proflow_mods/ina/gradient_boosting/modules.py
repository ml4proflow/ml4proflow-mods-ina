from __future__ import print_function, annotations
import pandas

from ml4proflow.modules import Module, DataFlowManager, SourceModule
from . import gradient_boosting


class GradientBoostingModule(Module):
    def __init__(self, dfm: DataFlowManager, config):
        Module.__init__(self, dfm, config)
        self.gradient_boosting = gradient_boosting.GradientBoosting()
        self.config = config
        self.config.setdefault('mode', 'Online')
        self.config.setdefault('process_config', 'config.json')
        self.config_range['mode'] = ['Training', 'Online']

    def on_new_data(self, name: str, sender: SourceModule, data: pandas.DataFrame):
        if self.config['mode'] == 'Online':
            if self.config['process_config'] is not None:
                self.gradient_boosting = gradient_boosting.GradientBoosting()
                self.gradient_boosting.set_data(dataframe=data)
                self.gradient_boosting.set_config(self.config['process_config'])
                result = self.gradient_boosting.run()
                for channel in self.config['channels_push']:
                    self._push_data(channel, result)
        elif self.config['mode'] == 'Training':
            self.gradient_boosting.set_data(dataframe=data)
        else:
            raise ValueError('Mode must be run or configuration.')

    @classmethod
    def get_module_desc(cls):
        return {"name": "Gradient Boosting",
                "categories": ["Classification"],
                "jupyter-gui-cls": "ml4proflow_mods.ina.widgets.gradient_boosting_widget.GradientBoostingWidget",
                "jupyter-gui-override-settings-type": {'process_config': 'file'},
                "html-description": """
                <H1 style="margin-bottom:6px;margin-top:6px">Gradient Boosting</H1>
                <h2 style="margin-bottom:1pt">Kurzbeschreibung:   <button type="button">Handbuch</button></h2><p>Todo</p><h2 style="margin-bottom:1pt">Nutzen:</h2><p>Todo</p>
                <h2 style="margin-bottom:1pt">Geeignete Maschinentypen:</h2><ul><li>Bisher keine Einschränkungen bekannt.</li></ul>
                """
                }
