# -*- coding: utf-8 -*-
"""
Created on Mon Jun 28 12:21:33 2021

@author: bucfra

model learner for mini batches after DTW, optional with filter
learn from data means and standard deviation for each point
then compare new data to this and count how often new data points are out the area

"""

import pandas as pd
import numpy as np
import statistics as st
from sklearn.model_selection import train_test_split
import sys

from .base_process import BaseProcess


class BatchMatches(BaseProcess):
    """This class handles all functions necessary to apply the Statistical Analysis Process to data.

    Attributes:
        data: Pandas dataframe containing the process data which is to be analyzed
        config: Python dictionary containing all parameters to reapply inference on a trained model
        traindata: Pandas dataframe set in read_and_split_data().
        testdata: Pandas dataframe set in read_and_split_data().
        anomalychance_list: Inferred anomalies on self.data, set in infer_anomaly().
        anomalychance_list_test_data: Inferred anomalies on test data while training in fit_model().

    """
    def __init__(self, dataframe: pd.DataFrame = None, config: {} = None):
        super().__init__(dataframe, config)
        self.traindata = None
        self.testdata = None
        self.anomalychance_list = None
        self.anomalychance_list_test_data = None

    def read_and_split_data(self, split=True) -> pd.DataFrame:
        """This function takes the dataframe from self.data and segments it along the period column.

        If the parameter split is set to true, the segmented data is divided into a train and a test validation set.

        Args:
            split: Boolean value specifying if returned dataframe is splitted into training and test data

        Raises:
            ValueError: If no data is set or data is not segmented (has no column period)

        Returns:
            Returns a pandas dataframe in the right format to fit a model to. If split==True, two dataframes, one for
            training and one for validation, are returned.
        """
        if self.data is None or 'period' not in self.data.columns:
            raise ValueError("Please make sure a segmented dataframe can be used for training")

        final_dataframe = pd.DataFrame()
        unique_patterns = np.sort(self.data['period'].unique())

        for batch_number, pattern_number in enumerate(unique_patterns):
            if pattern_number == -1:
                continue
            tmp_data = self.data.loc[self.data['period'] == pattern_number].copy()
            tmp_data.drop(['period'], axis='columns', inplace=True)
            final_dataframe[str(batch_number)] = tmp_data.reset_index(drop=True)

        final_dataframe = final_dataframe.transpose()

        if split:
            return train_test_split(final_dataframe, test_size=0.2)

        return final_dataframe

    def fitting_model(self): # -> tuple[[], []]: (not supported <3.9)
        """This function calculates the upper and lower bounds for the model from the train data.

        Raises:
            ValueError: If traindata is not set correctly.

        Returns:
            The upper and lower bounds of the model.
        """
        if self.traindata is None:
            raise ValueError('Please make sure training data is set correctly.')

        meanlist = self.traindata.apply(np.mean, axis=0)
        stdlist = self.traindata.apply(np.std, axis=0)

        upper = meanlist + stdlist
        lower = meanlist - stdlist

        return upper, lower

    def sign_check(self, obs: []) -> []:
        """Method to check if a time series observation lies between two other time series.

        All time series data must be of the same size.

        Args:
            obs: 1d-array, observation signal. Need to be same size as reference signals.

        Raises:
            ValueError: If model configuration is not set in self.config.

        Returns:
            Float, anomaly chance of the given observation and the bounding.

        """
        if 'upper' not in self.config or 'lower' not in self.config:
            raise ValueError('Model must be trained or loaded before sign check.')

        upper_bound_check = (self.config['upper'] - obs) >= 0
        lower_bound_check = (obs - self.config['lower']) >= 0

        correctness = upper_bound_check == lower_bound_check

        anomaly_chance = np.round(np.sum(correctness == False) / len(self.config['upper']), 4)

        return anomaly_chance

    def fit_model(self) -> {}:
        """Takes data, splits it into train and test, saves the model parameters to config and returns model performance
        measures on the testdata.

        Returns:
            model_stats: A python dictionary containing the min, max, median, mean and standard_dev of the anomaly
            chance of the testdata.
        """
        self.traindata, self.testdata = self.read_and_split_data()
        self.config['upper'], self.config['lower'] = self.fitting_model()

        self.anomalychance_list_test_data = []
        for i in range(0, len(self.testdata)):
            anomaly_value = self.sign_check(self.testdata.iloc[i])
            self.anomalychance_list_test_data.append(anomaly_value)

        model_stats = {'min': min(self.anomalychance_list_test_data), 'max': max(self.anomalychance_list_test_data),
                       'median': round(st.median(self.anomalychance_list_test_data), 4),
                       'mean': round(st.mean(self.anomalychance_list_test_data), 4),
                       'standard_dev': round(np.std(self.anomalychance_list_test_data), 4)}

        return model_stats

    def infer_anomaly(self) -> []:
        """This function calculates the anomaly chance for the current self.data.

        Returns:
            A list of all anomaly chances.
        """
        self.traindata = self.read_and_split_data(split=False)
        self.anomalychance_list = []

        for i in range(0, len(self.traindata)):
            self.anomalychance_list.append(self.sign_check(self.traindata.iloc[i]))

        return self.anomalychance_list

    def write_config(self, file_name: str = 'statistical_analysis.json') -> str:
        self.config['upper'] = self.config['upper'].to_list()
        self.config['lower'] = self.config['lower'].to_list()
        return super(BatchMatches, self).write_config(file_name)

    def run(self, run_params: list = None) -> []:
        super(BatchMatches, self).run(run_params)
        return self.infer_anomaly()


if __name__ == '__main__':
    analyzer = BatchMatches()
    analyzer.run(sys.argv[1:])
