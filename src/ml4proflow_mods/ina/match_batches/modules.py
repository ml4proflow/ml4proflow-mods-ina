from __future__ import print_function, annotations
import pandas

from ml4proflow.modules import Module, DataFlowManager, SourceModule
from . import match_batches


class MatchBatchesModule(Module):
    def __init__(self, dfm: DataFlowManager, config):
        Module.__init__(self, dfm, config)
        self.batch_matching = match_batches.BatchMatches()
        self.config = config
        self.config.setdefault('mode', 'Online')
        self.config.setdefault('process_config', 'config.json')
        self.config_range['mode'] = ['Training', 'Online']

    def on_new_data(self, name: str, sender: SourceModule, data: pandas.DataFrame):
        if self.config['mode'] == 'Online':
            if self.config['process_config'] is not None:
                self.batch_matching = match_batches.BatchMatches()
                self.batch_matching.set_data(dataframe=data)
                self.batch_matching.set_config(self.config['process_config'])
                result = self.batch_matching.run()
                for channel in self.config['channels_push']:
                    self._push_data(channel, result)
        elif self.config['mode'] == 'Training':
            self.batch_matching.set_data(dataframe=data)
            self._training_gui_handler(2)
        else:
            raise ValueError('Mode must be run or configuration.')

    @classmethod
    def get_module_desc(cls):
        return {"name": "Match Batches",
                "categories": ["Classification"],
                "jupyter-gui-cls": "ml4proflow_mods.ina.widgets.match_batches_widget.MatchBatchesWidget",
                "jupyter-gui-override-settings-type": {'process_config': 'file'},
                "html-description": """
                <H1 style="margin-bottom:6px;margin-top:6px">Match Batches</H1>
                <h2 style="margin-bottom:1pt">Kurzbeschreibung:   <button type="button">Handbuch</button></h2><p>Das Batch Match Verfahren erlernt das Normalverhalten repetetiver Prozesse indem das Normalverhalten durch upper und lower bounds eingegrenzt wird. Wird ein neuer Durchlauf überprüft, so kann eine Einschätzung gegeben werden, ob starke Abweichungen bezüglich des Normalverhaltens auftreten.</p><h2 style="margin-bottom:1pt">Nutzen:</h2><p>Es besteht die Möglichkeit, auch zuvor unbekannte Fehler zu erkennen, da sich der Modellierungsprozess nur auf die Gutdaten bezieht.</p>
                <h2 style="margin-bottom:1pt">Geeignete Maschinentypen:</h2><ul><li>Bisher keine Einschränkungen bekannt.</li></ul>
                """
                }
