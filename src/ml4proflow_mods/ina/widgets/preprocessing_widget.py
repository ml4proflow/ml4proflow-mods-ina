from __future__ import print_function, annotations
import ipywidgets as widgets
from ipywidgets import GridspecLayout, DOMWidget
import plotly.graph_objects as go
import pylab as plt

from ml4proflow_jupyter.widgets import BasicWidget


class PreprocessingWidget(BasicWidget):
    """This class handles the graphical user interface for the preprocessing process.
    """
    def __init__(self, module):
        BasicWidget.__init__(self, module)
        self.path_to_config = None
        self.wrapper_configuration = {'params': None, 'restrict_plotting_data': 300000, 'restrict_plotting_columns': 50}
        self.training_gui = GridspecLayout(7, 4)
        self.online_gui = GridspecLayout(3, 1)
        
    def __str__(self):
        return 'Preprocessing'
        
    def create_online_gui(self) -> widgets.GridspecLayout:
        """Function returns layout for online mode of preprocessing process.

        This layout contains the button On Show Last Data which plots the raw input data and preprocessed data next to each
        other and adds those plots to the online layout.

        Returns:
            self.online_gui of type GridspecLayout which contains the show last data button and two plots: input and
            output of online process.
        """

        show_last_data_button = widgets.Button(description='Show last data', disabled=False)

        def _plot_raw_data():
            placeholder = widgets.IntSlider(value=0, min=0, max=1)

            def _make_plot(placeholder_in):
                plt.plot(self.module.preprocessor.data)

            out = widgets.interactive_output(_make_plot, {'placeholder_in': placeholder})

            return widgets.VBox([out])

        def _plot_output_data():
            placeholder = widgets.IntSlider(value=0, min=0, max=1)

            def _make_plot(placeholder_in):
                plt.plot(self.module.preprocessor.preprocessed_data)

            out = widgets.interactive_output(_make_plot, {'placeholder_in': placeholder})

            return widgets.VBox([out])

        def _on_show_last_data_button_clicked(b):
            self.online_gui[0, :] = widgets.VBox([show_last_data_button, widgets.HBox([_plot_raw_data(), _plot_output_data()])])
        show_last_data_button.on_click(_on_show_last_data_button_clicked)
        self.online_gui[0, :] = widgets.VBox([show_last_data_button])

        return self.online_gui
    
    def create_training_gui(self) -> widgets.GridspecLayout:
        """Function returns Layout for configuration mode of preprocessing process.

        Returns:
            self.training_gui of Type GridspecLayout which contains all GUI elements necessary to create a config file
            for the preprocessing process.
        """
        self._configure_data_in()
        return self.training_gui

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Training', self.create_training_gui(),), ('Online', self.create_online_gui(), )]

    def _configure_data_in(self) -> None:
        """Function creates a button to load data from internal pipeline and adds it to the pipeline.
        """
        wait_text = widgets.HTML('<p>Waiting for data, please refresh manually.</p>')
        refresh_button = widgets.Button(description='Refresh', disabled=False)

        def _on_refresh_button_clicked(b):
            if self.module.preprocessor.data is not None:
                self._plot_raw_data()

        refresh_button.on_click(_on_refresh_button_clicked)

        self.training_gui[0, :] = widgets.VBox([wait_text, refresh_button])

    def _plot_raw_data(self) -> None:
        """Function plots the raw input data from the pipeline and adds this plot to self.training_gui.
        """
        self.training_gui[:, :] = widgets.VBox([])
        if 'source_name' in self.module.preprocessor.attrs and self.module.preprocessor.attrs['source_name'] is not None:
            self.training_gui[0, :] = widgets.HTML('<h1>Configuring on data from: ' +
                                                   self.module.preprocessor.attrs['source_name'].split('\\')[-1] + '</h1>')
        else:
            self.training_gui[0, :] = widgets.HTML('<h1>Showing data from pipeline</h1>')

        def _plot_data():
            fig = go.FigureWidget()
            for column_counter, column in enumerate(self.module.preprocessor.data.columns.values):
                if column_counter > self.wrapper_configuration['restrict_plotting_columns']:
                    break
                fig.add_traces(go.Scatter(y=self.module.preprocessor.data.loc[0:self.wrapper_configuration['restrict_plotting_data'], column], mode='lines', name=column))
            fig.show()

        out = widgets.interactive_output(_plot_data, {})
        self.training_gui[1, :] = out
        self._configure_preprocessing()

    def _configure_additional_preprocessing_parameters(self, change: str):
        """Function generates additional selections if required by certain preprocessing methods.

        Args:
            change: String representing the chosen preprocessing method.
        """
        def on_change(tmp_params):
            self.wrapper_configuration['params'] = tmp_params
        method = change
        ret = widgets.HTML(' ')
        if method == 'Filter':
            filter_selector = widgets.Dropdown(options=['lowpass', 'highpass'], disabled=False)
            filter_selector.observe(on_change)
            ret = filter_selector
        display(ret)

    def _configure_preprocessing(self):
        """Function provides all GUI elements necessary to choose data and a preprocessing method and adds those to the
        self.training_gui.
        """
        info_text = widgets.HTML('')

        def _plot_preprocessed_data(*args):
            fig = go.FigureWidget()
            for column_counter, column in enumerate(self.module.preprocessor.temporary_out.columns.values):
                if column_counter > self.wrapper_configuration['restrict_plotting_columns']:
                    break
                fig.add_traces(go.Scatter(y=self.module.preprocessor.temporary_out.loc[0:self.wrapper_configuration['restrict_plotting_data'], column], mode='lines', name=column))
            fig.show()

        columns_description = widgets.HTML('Choose data to apply method to')
        columns_source = widgets.RadioButtons(options=['Raw data', 'Preprocessed data'], description='Choose source', disabled=False)

        columns_selector = widgets.SelectMultiple(options=self.module.preprocessor.data.columns.values, disabled=False)

        methods_description = widgets.HTML('Choose method')
        methods_selector = widgets.Dropdown(options=self.module.preprocessor.get_methods(), disabled=False)
        options = widgets.interactive_output(self._configure_additional_preprocessing_parameters, {'change': methods_selector})

        apply_method_button = widgets.Button(description='Apply method', disable=False)

        def _on_apply_method_clicked(b):
            self.module.preprocessor.apply_method(columns_source.value, list(columns_selector.value), methods_selector.value, self.wrapper_configuration['params'])
            self.training_gui[3, :] = widgets.interactive_output(_plot_preprocessed_data, {})
            self._configure_output_data()
            info_text.value = '<p>The following plot shows the preprocessed signal. If you would like to add it to the final output of the preprocessing, please click the Add to oputput button.</p>'

        apply_method_button.on_click(_on_apply_method_clicked)

        def _update_column_options(v):
            if columns_source.value == 'Raw data':
                columns_selector.options = self.module.preprocessor.data.columns.values
            else:
                columns_selector.options = self.module.preprocessor.preprocessed_data.columns.values

        # Makes options for column dependent on columns source
        columns_source.observe(_update_column_options, 'value')

        self.training_gui[2, :] = widgets.VBox([widgets.HBox([widgets.VBox([columns_description, columns_source, columns_selector]), widgets.VBox([methods_description, methods_selector]), widgets.VBox([apply_method_button]), options]), info_text])

    def _configure_output_data(self) -> None:
        """Function provides GUI elements necessary to add preprocessed signals to the final output and adds those to
        self.training_gui.
        """
        column_name = widgets.Text(description='Signal name: ', disabled=False, value=list(self.module.preprocessor.temporary_out.columns)[0])
        add_to_output_button = widgets.Button(description='Add to output', disabled=False)
        output_button = widgets.Button(description='Output', disabled=False)
        save_config_button = widgets.Button(description='Save Config', disabled=False)

        info_text = widgets.HTML('')

        def _plot_output_data(*args):
            fig = go.FigureWidget()
            for column_counter, column in enumerate(self.module.preprocessor.preprocessed_data.columns.values):
                if column_counter > self.wrapper_configuration['restrict_plotting_columns']:
                    break
                fig.add_traces(go.Scatter(y=self.module.preprocessor.preprocessed_data.loc[0:self.wrapper_configuration['restrict_plotting_data'], column], mode='lines', name=column))
            fig.show()

        def _on_save_config_button_clicked(b):
            self.module.preprocessor.write_config()

        def _on_add_to_output_button_clicked(b):
            self.module.preprocessor.add_to_output(column_name=column_name.value)
            info_text.value = '<p>The following plot shows the final result of the currently configured preprocessing process.</p>'
            info_text_2 = widgets.HTML('<p>To push the current preprocessed data into the toolbox pipeline, please click the Output button. To save the config to the file system, please click the Save config button.</p>')
            out2 = widgets.interactive_output(_plot_output_data, {})
            self.training_gui[5, :] = out2
            self.training_gui[6, :] = widgets.VBox([info_text_2, widgets.HBox([output_button, save_config_button])])

        def _on_output_button_clicked(b):
            for channel in self.module.config['channels_push']:
                self.module._push_data(channel, self.module.preprocessor.preprocessed_data)

        save_config_button.on_click(_on_save_config_button_clicked)
        add_to_output_button.on_click(_on_add_to_output_button_clicked)
        output_button.on_click(_on_output_button_clicked)

        self.training_gui[4, :] = widgets.VBox([widgets.HBox([column_name, add_to_output_button]), info_text])
