from __future__ import print_function, annotations
from ipywidgets import GridspecLayout, DOMWidget
import ipywidgets as widgets
import pandas as pd
from ipyfilechooser import FileChooser
from pathlib import Path
import pylab as plt

from ml4proflow_jupyter.widgets import BasicWidget


class MatchBatchesWidget(BasicWidget):
    """This class handles the graphical user interface for the match batches process.

    Attributes:
        analyzer: The match batches object used calculations throughout this GUI.
        wrapper_configuration: A python dictionary containing parameters corresponding to the visualization.
        path_to_config: Path on computer to the config if set.
        training_gui: A widgets.GridspecLayout containing all GUI elements relevant for the configuration of the
        process.
        live_layout: A widgets.GridspecLayout containing all GUI elements pertaining to the live evaluation of the
        statistical analysis process.
        parent:
    """
    def __init__(self, module):
        """Inits the batch matches GUI given a module object.

        Args:
            module: A module object forming the interface to the toolbox module.
        """
        BasicWidget.__init__(self, module)
        self.wrapper_configuration = {}
        self.path_to_config = None
        self.training_gui = GridspecLayout(1, 1)
        self.live_layout = GridspecLayout(1, 1)

    def __str__(self):
        return 'Batch Matches'

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Ergebnisse (Training)', self.create_training_gui(),), ('Ergebnisse (Online)', self.create_online_gui(), )]

    def on_show_last_data(self, button: widgets.Button) -> widgets:
        """Function called after pressing show last data button and returns a layout containing the plotted input and
        plotted output data.

        Args:
            button

        Returns:
            self.live_layout of type GridspecLayout which contains two plots: input and output of online process
        """
        self.live_layout[0, :] = widgets.VBox([self._plot_new_data_in()])
        return self.live_layout

    def _plot_new_data_in(self) -> widgets:
        """Function that plots 1. The current analyzer train data and configuration and 2. The anomaly chances of that
        train data. It returns a widget containing those two plots.

        Returns:
            A widget containing the plots of the current analyzer train data and the anomaly chances.
        """
        placeholder = widgets.IntSlider(value=0, min=0, max=1)

        def _make_plot_train(placeholder_in):
            fig, ax = plt.subplots()
            for index, values in self.module.batch_matching.traindata.iterrows():
                ax.plot(values)
            ax.plot(self.module.batch_matching.config['upper'], color='red', label='Upper Bound')
            ax.plot(self.module.batch_matching.config['lower'], color='red', label='Lower Bound')
            display(fig)

        def _make_plot_chance_anomaly(placeholder_in):
            fig, ax = plt.subplots()
            ax.scatter([*range(0, len(self.module.batch_matching.anomalychance_list))], self.module.batch_matching.anomalychance_list, color='orange', label='Chance Anomaly')
            display(fig)

        out = widgets.interactive_output(_make_plot_train, {'placeholder_in': placeholder})
        out2 = widgets.interactive_output(_make_plot_chance_anomaly, {'placeholder_in': placeholder})

        return widgets.VBox([out, out2])

    def create_training_gui(self) -> widgets:
        """Function starts the training GUI handler and returns the training GUI layout.

        Returns:
            self.training_gui of type GridspecLayout containing the GUI for the analyzer training mode.
        """
        self._training_gui_handler(1)
        return self.training_gui

    def _training_gui_handler(self, state: int = None) -> None:
        """Function handles the Gridspec Layout of the training GUI.

        Args:
            state: New state of training configuration state machine.
        """
        if state is not None:
            self.wrapper_configuration['state'] = state

        if self.wrapper_configuration['state'] == 1:
            self.training_gui[0, :] = self._configure_data_in()
        elif self.wrapper_configuration['state'] == 2:
            self.training_gui[0, :] = self._plot_trained_model()
            self._training_gui_handler(3)
        elif self.wrapper_configuration['state'] == 3:
            self.training_gui[0, :] = widgets.VBox([self.training_gui[0, :], self._save_config_utils()])

    def _plot_trained_model(self) -> widgets:
        """This functions trains the analyzer model and returns three plots visualizing the training result.

        1. A plot of the train data and the calculated model bounds.
        2. A plot of the validation data and the model bounds of the training data.
        3. A plot of the anomaly chances of the validation data.

        Returns:
            A widget containing those three plots.
        """
        model_stats = self.module.batch_matching.fit_model()
        placeholder = widgets.IntSlider(value=0, min=0, max=1)

        def _make_plot_train(placeholder_in):
            fig, ax = plt.subplots()
            for index, values in self.module.batch_matching.traindata.iterrows():
                ax.plot(values)
            ax.plot(self.module.batch_matching.config['upper'], color='red', label='Upper Bound')
            ax.plot(self.module.batch_matching.config['lower'], color='red', label='Lower Bound')
            display(fig)

        def _make_plot_test(placeholder_in):
            fig, ax = plt.subplots()
            for index, values in self.module.batch_matching.testdata.iterrows():
                ax.plot(values)
            ax.plot(self.module.batch_matching.config['upper'], color='red', label='Upper Bound')
            ax.plot(self.module.batch_matching.config['lower'], color='red', label='Lower Bound')
            display(fig)

        # TODO: match colors of test data and scatter plot of test data
        def _make_plot_chance_anomaly(placeholder_in):
            fig, ax = plt.subplots()
            ax.scatter([*range(0, len(self.module.batch_matching.anomalychance_list_test_data))], self.module.batch_matching.anomalychance_list_test_data, color='orange', label='Chance Anomaly')
            display(fig)

        out = widgets.interactive_output(_make_plot_train, {'placeholder_in': placeholder})
        out2 = widgets.interactive_output(_make_plot_test, {'placeholder_in': placeholder})
        out3 = widgets.interactive_output(_make_plot_chance_anomaly, {'placeholder_in': placeholder})

        return widgets.HBox([out, out2, out3])

    def create_online_gui(self) -> widgets:
        """Function returns layout for online mode of batch matching process.

        Returns:
            self.live_layout of type GridspecLayout displaying the text Waiting for input data.
        """
        self.live_layout[0, :] = widgets.HTML(value='Waiting for input data')
        return self.live_layout

    def _configure_data_in(self) -> widgets:
        """Function creates the GUI elements necessary to select a data file to read.

        Returns:
            Widget containing text and the file selection widget
        """
        text = widgets.HTML('<p>Please choose csv file to read training data from.</p>')
        fc = FileChooser(str(Path.cwd().parent.parent))

        def _on_file_selected(chooser):
            self.module.batch_matching.set_data(path=fc.selected)
            self._training_gui_handler(2)

        fc.register_callback(_on_file_selected)
        return widgets.VBox([text, fc])

    def _save_config_utils(self) -> widgets:
        """Function generates and returns all GUI items necessary to save the config.

        Returns:
            A widget containing these GUI elements.
        """
        config_path = [widgets.HTML(value='')]

        button_write_config = widgets.Button(description='Write config', disabled=False)

        def _button_write_config_clicked(b):
            path = self.module.batch_matching.write_config()
            config_path[0].value = 'Config saved to: ' + str(path)

        button_write_config.on_click(_button_write_config_clicked)

        return widgets.VBox([widgets.HBox([button_write_config]), config_path[0]])
