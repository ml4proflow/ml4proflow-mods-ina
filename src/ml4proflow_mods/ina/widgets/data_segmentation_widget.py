from __future__ import print_function, annotations
from ipywidgets import GridspecLayout, DOMWidget
import ipywidgets as widgets
import numpy as np
import seaborn as sns
import plotly.graph_objects as go
import pylab as plt


from ml4proflow_jupyter.widgets import BasicWidget


class DataSegmentationWidget(BasicWidget):
    """This class handles the graphical user interface for the data segmentation process.
    """
    def __init__(self, module):
        """Inits the data segmentation GUI given a datastore object.
        """
        BasicWidget.__init__(self, module)
        self.wrapper_configuration = {'restrict_plotting_data': 300000, 'filter_multiples': True, 'mark_params': {},
                                      'state': 1, 'push_option': 1}
        self.path_to_config = None
        self.return_point = None
        self.training_gui = GridspecLayout(1, 1)
        self.live_layout = GridspecLayout(1, 1)

    def __str__(self):
        return 'Segmentation'

    def _plot_new_data_in(self) -> widgets:
        """Function that plots the current segmenter train data and returns a widget containing that plot.

        Returns:
            A widget containing the plot of the current segementer train data
        """
        placeholder = widgets.IntSlider(value=0, min=0, max=1)

        def _make_plot(placeholder_in):
            plt.plot(self.module.segmenter.train_data, color='black')

        out = widgets.interactive_output(_make_plot, {'placeholder_in': placeholder})

        return widgets.VBox([out])

    def _plot_marked_data(self) -> widgets:
        """Function that plots the current segmenter train data, color coded by segments created by the segmenter.

        Returns:
            A widget containing the plot of the current segmenter train data, color coded by segment
        """
        info_text = widgets.HTML('<p>The following plot shows the original signal, where each segment is marked in a different colour.</p>')
        placeholder = widgets.IntSlider(value=0, min=0, max=1)

        def _make_plot(placeholder_in):
            patterns_plt = np.sort(self.module.segmenter.data['period'].unique())

            rgb_values = sns.color_palette("Set2", len(patterns_plt))
            color_map = dict(zip(patterns_plt, rgb_values))

            fig, ax = plt.subplots()
            indices = self.module.segmenter.data.index.to_numpy()
            ax.plot(indices, np.concatenate(self.module.segmenter.train_data, axis=0)[indices], color='black')
            for value in patterns_plt:
                if value == -1:
                    pass
                else:
                    indices = self.module.segmenter.data.index[self.module.segmenter.data['period'] == value].to_numpy()
                    indices = indices[indices < np.concatenate(self.module.segmenter.train_data, axis=0).shape[0]]
                    ax.plot(indices, np.concatenate(self.module.segmenter.train_data, axis=0)[indices], color=color_map[value])

        out = widgets.interactive_output(_make_plot, {'placeholder_in': placeholder})

        return widgets.VBox([info_text, out])

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Training', self.create_training_gui(),), ('Online', self.create_online_gui(), )]

    def create_training_gui(self) -> widgets:
        """Function starts the training GUI handler and returns the training GUI layout.

        Returns:
            self.training_gui of type GridspecLayout containing the GUI for the segmenter configuration mode.
        """
        self._training_gui_handler()
        return self.training_gui

    def _training_gui_handler(self, state: int = None) -> None:
        """Function handles the Gridspec Layout of the training GUI.
        """
        if state is not None:
            self.wrapper_configuration['state'] = state

        if self.wrapper_configuration['state'] == 1:
            self.training_gui[0, :] = self._configure_data_in()
        elif self.wrapper_configuration['state'] == 2:
            self.training_gui[0, :] = self._configure_training_data()
            self._training_gui_handler(3)
        elif self.wrapper_configuration['state'] == 3 and self.return_point is None:
            self.return_point = widgets.VBox([self.training_gui[0, :], self._choose_method()])
            self.training_gui[0, :] = self.return_point
        elif self.wrapper_configuration['state'] == 3 and self.return_point is not None:
            self.training_gui[0, :] = self.return_point
        elif self.wrapper_configuration['state'] == 4:
            self.training_gui[0, :] = widgets.VBox([self.training_gui[0, :], self._detect_periodicities()])
        elif self.wrapper_configuration['state'] == 5:
            self.training_gui[0, :] = widgets.VBox([self.training_gui[0, :], self._identify_patterns()])
        elif self.wrapper_configuration['state'] == 6:
            self.training_gui[0, :] = widgets.VBox([self.training_gui[0, :], self._plot_marked_data(),
                                                    self._save_config_utils()])
        elif self.wrapper_configuration['state'] == 7:
            self.training_gui[0, :] = widgets.VBox([self.training_gui[0, :], self._slice_by_peaks()])

    def create_online_gui(self) -> widgets:
        """Function returns layout for online mode of segmenter process.

        This layout contains the button On Show Last Data which plots the training data and segmented data next to each
        other and adds those plots to the live layout.

        Returns:
            self.live_layout of type GridspecLayout which contains the show last data button and two plots: input and
            output of online process.
        """
        show_last_data_button = widgets.Button(description='Show last data', disabled=False)

        def _on_show_last_data_button_clicked(b):
            self.live_layout[0, :] = widgets.VBox([show_last_data_button, widgets.HBox([self._plot_new_data_in(),
                                                                                        self._plot_marked_data()])])

        show_last_data_button.on_click(_on_show_last_data_button_clicked)
        self.live_layout[0, :] = widgets.VBox([show_last_data_button])

        return self.live_layout

    def _configure_data_in(self) -> widgets:
        """Function creates a button to load data from internal pipeline.

        Returns:
            Widget containing info_text and refresh button.
        """
        wait_text = widgets.HTML('<p>Waiting for data, please refresh manually.</p>')
        refresh_button = widgets.Button(description='Refresh', disabled=False)

        def _on_refresh_button_clicked(b):
            if self.module.segmenter.data is not None:
                self._training_gui_handler(2)

        refresh_button.on_click(_on_refresh_button_clicked)

        return widgets.VBox([wait_text, refresh_button])

    def _configure_training_data(self) -> widgets:
        """Function configures data columns for segmentation based on user input.

        Returns:
            A plot of the configured columns and a columns selection widget.
        """
        self.training_gui[:, :] = widgets.VBox([])
        if 'source_name' in self.module.segmenter.attrs and self.module.segmenter.attrs['source_name'] is not None:
            self.training_gui[0, :] = widgets.HTML('<h1>Configuring Training on: ' +
                                                   self.module.segmenter.attrs['source_name'].split('\\')[-1] + '</h1>')
        else:
            self.training_gui[0, :] = widgets.HTML('<h1>Configuring Training on buffered data</h1>')

        info_text = widgets.HTML('<h2>Data segmentation configuration</h2><p>This view leads through the configuration of a data segmentation process. For the first step, please take a look at the plot and select at least one column in the right selection view to segment the data by. Multiple columns can be selected by holding the control key and clicking on them.</p>')

        columns = widgets.SelectMultiple(
            options=self.module.segmenter.get_data_header(),
            disabled=False
        )

        def _plot_raw_training_data(columns):
            if not columns:
                columns = self.module.segmenter.get_data_header()

            self.module.segmenter.set_training_data(columns)

            raw_train_data_plot = go.Figure()
            for column in columns:
                raw_train_data_plot.add_traces(
                    go.Scatter(
                        y=self.module.segmenter.data.loc[0:self.wrapper_configuration['restrict_plotting_data'], column],
                        mode='lines', name=column))
            raw_train_data_plot.show()

        out = widgets.interactive_output(_plot_raw_training_data, {'columns': columns})
        text = widgets.HTML('<p>Please select signal to segment by:</p>')

        return widgets.VBox([info_text, widgets.HBox([out, widgets.VBox([text, columns])])])

    def _choose_method(self) -> widgets:
        """Functions creates the GUI element necessary to choose a method for data segmentation and starts the next GUI
        element creation based on the selection.

        Returns:
            A dropdown menu of available segmentation options.
        """
        method = widgets.Dropdown(
            options=[('Please choose', 1), ('Detect periodicities', 2), ('Identify recurring patterns', 3),
                     ('Slice by peaks', 4)],
            value=1,
            description='Method: '
        )

        info_text = widgets.HTML('<p>To continue, please choose a method for segmentation: (Detect periodicities starts automatically after selection)</p>')

        def _call_method(method):
            if method == 2:
                self._training_gui_handler(4)
            elif method == 3:
                self._training_gui_handler(5)
            elif method == 4:
                self._training_gui_handler(7)

        out = widgets.interactive_output(_call_method, {'method': method})
        return widgets.VBox([info_text, widgets.HBox([method, out])])

    def _identify_patterns(self) -> widgets:
        """Function creates the GUI elements associated with selecting parameters and displaying patterns for manual
        pattern selection.

        Returns:
            All widgets necessary to identify and select an identified pattern.
        """
        self.wrapper_configuration['method'] = 'patterns'
        self.wrapper_configuration['mark_params'] = {}

        button_search_for_patterns = widgets.Button(description='Search for patterns', disabled=False)
        button_apply_to_data = self._mark_in_data()

        info_text_1 = widgets.HTML('<p>Please set the parameters in order to search for patterns in the chosen signal. Pattern length is necessary to start the search.<ul><li>Pattern length sets the length of the patterns that are returned by the search</li><li>Number of patterns sets the number of different patterns that are returned and plotted</li><li>Pattern to use is set after the search to define, which pattern is used to continue the process</li><li>Allowed deviation is also used when continuing the process by marking all pattern occurences in the signal and sets the allowed deviation from the sample pattern</li></ul></p>')
        info_text_2 = widgets.HTML('<p>When the pattern to continue is chosen, please click on the Apply to data button to search for this pattern occurences in the signal and plot them.</p>')

        starts_with_zero_checkbox = widgets.Checkbox(value=False, description='Pattern must start with zeroes',
                                                     disabled=False, indent=False)
        pattern_length = widgets.IntText(description='Pattern length: ', value=0, disabled=False)
        number_of_patterns = widgets.IntSlider(description='Number of patterns: ', value=1, min=1, max=10, step=1,
                                               disabled=False)
        similarity_slider = widgets.FloatSlider(value=0.05, min=0, max=1, step=0.05, description='Allowed deviation',
                                                disabled=False)

        chosen_pattern = widgets.IntSlider(description='Pattern to use: ', value=0, min=1, max=10, step=1,
                                           disabled=False)

        def _similarity_slider_on_change(value):
            self.wrapper_configuration['mark_params']['percentile'] = value['new']

        def _plot_found_patterns(pattern_length, number_of_patterns, starts_with_zero):
            if pattern_length == 0:
                return
            global patterns
            patterns = self.module.segmenter.identify_patterns(pattern_length=pattern_length, num_patterns=number_of_patterns,
                                                        start_with_zero=starts_with_zero)

            patterns_plt = list(patterns.keys())
            button_apply_to_data.disabled = False
            if len(patterns_plt) < 1:
                return
            elif len(patterns_plt) == 1:
                plt.plot(patterns[patterns_plt[0]])
                return

            fig, axs = plt.subplots(1, len(patterns.keys()), sharex=True, sharey=True)

            for counter, pattern_number in enumerate(patterns_plt):
                axs[counter].plot(patterns[pattern_number])

        def _on_chosen_pattern_change(change):
            if (change['new'] == 0) or (change['new'] > number_of_patterns.value):
                return
            self.module.segmenter.set_pattern(patterns[change['new'] - 1])

        chosen_pattern.observe(_on_chosen_pattern_change, names='value')
        similarity_slider.observe(_similarity_slider_on_change, names='value')

        out = widgets.interactive_output(_plot_found_patterns,
                                         {'pattern_length': pattern_length, 'number_of_patterns': number_of_patterns,
                                          'starts_with_zero': starts_with_zero_checkbox})

        return widgets.VBox([info_text_1, widgets.VBox([pattern_length, starts_with_zero_checkbox, number_of_patterns, chosen_pattern, similarity_slider, widgets.HBox([button_search_for_patterns, button_apply_to_data]), out], layout=widgets.Layout(display='flex')), info_text_2])

    def _detect_periodicities(self) -> widgets:
        """Function that generates and returns all GUI items related to detecting periodicities and chosen one to
        segment the data accordingly.

        Returns:
            Widgets necessary, e.g. plot, slider to choose one, filter multiples checkbox, button to apply to data
        """
        self.wrapper_configuration['method'] = 'periodicities'
        self.wrapper_configuration['mark_params'] = {}

        info_text = widgets.HTML('<p>The found periodicities can be varied using the slider below. Filter multiples filters multiples of the same number from this selection. After moving the slider to an appropriate number, please click the Apply to data button to segment and plot the signal.</p>')

        params = {'method': 'FFT', 'method_params': {'period_range_tolerance': 4,
                                                     'filter_periods': self.wrapper_configuration['filter_multiples']}}
        periodicities, ratings = self.module.segmenter.find_periodicities(**params)

        button_apply_to_data = self._mark_in_data()

        slider = widgets.SelectionSlider(options=periodicities[:20], value=periodicities[0])
        filter_multiples_checkbox = widgets.Checkbox(value=self.wrapper_configuration['filter_multiples'],
                                                     description='Filter multiples', disabled=False, indent=False)

        def _on_filter_multiples(filter_bool):
            if filter_bool != self.wrapper_configuration['filter_multiples']:
                self.wrapper_configuration['filter_multiples'] = filter_bool
                self._detect_periodicities()

        def _custom_plot(x_val):
            self.wrapper_configuration['mark_params'] = {'periodicity': int(x_val)}

            periodicity_plot = go.Figure()
            periodicity_plot.add_traces(go.Scatter(y=np.concatenate(self.module.segmenter.train_data, axis=0), mode='lines'))

            button_apply_to_data.disabled=False

            v_lines = np.arange(0, len(np.concatenate(self.module.segmenter.train_data, axis=0)), x_val)
            for line in v_lines:
                periodicity_plot.add_vline(x=line)
            periodicity_plot.show()

        out = widgets.interactive_output(_custom_plot, {'x_val': slider})
        filter_multiples_checkbox.observe(_on_filter_multiples)

        return widgets.VBox([info_text, widgets.HBox([slider, filter_multiples_checkbox]), out, button_apply_to_data])

    def _slice_by_peaks(self) -> widgets:
        """Function that generates all GUI elements necessary to identify peaks in the data and slice accordingly.

        Returns: A VBox widget containing two slider and a plot of the peaks
        """
        self.wrapper_configuration['method'] = 'peaks'
        height_widget = widgets.FloatText(value=0, description='Peak height: ', disabled=False)
        distance_widget = widgets.FloatText(value=0, description='Peak distance: ', disabled=False)

        info_text = widgets.HTML('<p>This method searches for peaks in the signal. To start the search, please set at least one parameter: Either minimum peak height or minimum peak distance.</p>')
        info_text_2 = widgets.HTML('<p>When the segments seem right, please click Apply to data to segment the data.</p>')

        apply_to_data_button = self._mark_in_data()

        def _custom_plot(height, distance):
            if height == 0 and distance == 0:
                return
            peaks = self.module.segmenter.find_peaks(height, distance)

            periodicity_plot = go.Figure()
            periodicity_plot.add_traces(go.Scatter(y=np.concatenate(self.module.segmenter.train_data, axis=0), mode='lines'))

            apply_to_data_button.disabled = False

            v_lines = peaks
            for line in v_lines:
                periodicity_plot.add_vline(x=line)
            periodicity_plot.show()

        out = widgets.interactive_output(_custom_plot, {'height': height_widget, 'distance': distance_widget})

        return widgets.VBox([info_text, height_widget, distance_widget, out, info_text_2, apply_to_data_button])

    def _mark_in_data(self) -> widgets.Button:
        """Function returns a button, which if clicked, starts segmenting the data and then generates the plot and save
        config GUI elements.

        Returns:
            A button widget
        """
        button_apply = widgets.Button(description='Apply to data', disabled=True)

        def _on_button_apply_clicked(b):
            self.module.segmenter.mark_in_data(mark=self.wrapper_configuration['method'],
                                        mark_params=self.wrapper_configuration['mark_params'])

            self._training_gui_handler(6)

        button_apply.on_click(_on_button_apply_clicked)

        return button_apply

    def _save_config_utils(self) -> widgets:
        """Function generates and returns all GUI items necessary to add a pattern to a config and to save the config.

        Returns:
            A widget containing these GUI elements.
        """
        info_text = widgets.HTML('<p>If you want to save the process, please save it to the configuration using the Add to config button. If the final config should be written as a file, click the Write config button. This will save the configuration as data_segmentation_config.json in the working directory. It can then be selected for automated processing in the setting at the start of this tab.</p>')

        pattern_name = widgets.Text(description='Process name: ', disabled=False,
                                    value='pattern_' + str(self.module.segmenter.pattern_counter + 1))
        config_path = [widgets.HTML(value='')]

        button_add_to_config = widgets.Button(description='Add to config', disabled=False)
        button_write_config = widgets.Button(description='Write config', disabled=False)

        def _button_add_to_config_clicked(b):
            self.module.segmenter.save_to_config(name=str(pattern_name.value))

        def _button_write_config_clicked(b):
            path = self.module.segmenter.write_config()
            config_path[0].value = 'Config saved to: ' + str(path)

        button_add_to_config.on_click(_button_add_to_config_clicked)
        button_write_config.on_click(_button_write_config_clicked)

        return widgets.VBox([info_text, widgets.HBox([pattern_name, button_add_to_config, button_write_config]), config_path[0]])
