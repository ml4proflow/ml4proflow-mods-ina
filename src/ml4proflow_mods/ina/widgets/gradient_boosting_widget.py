from __future__ import print_function, annotations
from ipywidgets import GridspecLayout, DOMWidget
import ipywidgets as widgets

from ml4proflow_jupyter.widgets import BasicWidget


class GradientBoostingWidget(BasicWidget):
    """This class handles the graphical user interface for the gradient boosting process.

    Attributes:
        module.gradient_boosting: The gradient boosting object used for calculations throughout this GUI.
        wrapper_configuration: A python dictionary containing parameters corresponding to the visualization.
        path_to_config: Path on computer to the config if set.
    """
    def __init__(self, module):
        """Inits the batch matches GUI given a module object.

        Args:
            module: A module object forming the interface to the toolbox module.
        """
        BasicWidget.__init__(self, module)
        self.wrapper_configuration = {}
        self.training_gui = GridspecLayout(1, 1)
        self.online_gui = GridspecLayout(1, 1)

    def __str__(self):
        return 'Gradient Boosting'

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Training', self.create_training_gui(),), ('Online', self.create_online_gui(),)]

    def create_online_gui(self) -> widgets.GridspecLayout:
        """Function creates a gui containing one button to show the last predicted labels by the gradient boosting
        process.

        Returns:
            self.online_gui which is the GridspecLayout containing the refresh button and the label list.
        """
        label_text = widgets.HTML(value=str(self.module.gradient_boosting.pred_label_list))
        refresh_button = widgets.Button(description='Refresh', disabled=False)

        def _on_refresh_button_clicked(b):
            label_text.value = str(self.module.gradient_boosting.pred_label_list)

        refresh_button.on_click(_on_refresh_button_clicked)
        self.online_gui[:, :] = widgets.VBox([label_text, refresh_button])

        return self.online_gui

    def create_training_gui(self) -> widgets.GridspecLayout:
        """Function creates a gui containing a button to fit the model on data and the achieved accuracy.

        Returns:
            self.training_gui which is the GridspecLayout containing the fit model button, accuracy information text and
            the save config utils.
        """
        fit_model_button = widgets.Button(description='Fit model on data', disabled=False)
        info_text = widgets.HTML(value='')

        def _on_fit_model_button_clicked(b):
            if self.module.gradient_boosting.data is not None:
                acc, bal_acc = self.module.gradient_boosting.fit_model()
                info_text.value = '<p>Model accuracy: </p>' + str(acc)
                self.training_gui[:, :] = widgets.VBox([fit_model_button, info_text, self._save_config_utils()])
            else:
                info_text.value = 'No data available to train model on.'

        fit_model_button.on_click(_on_fit_model_button_clicked)
        self.training_gui[:, :] = widgets.VBox([fit_model_button, info_text])

        return self.training_gui

    def _save_config_utils(self) -> widgets:
        """Function generates and returns all GUI items necessary to save the config.

        Returns:
            A widget containing these GUI elements.
        """
        config_path = [widgets.HTML(value='')]

        button_write_config = widgets.Button(description='Write config', disabled=False)

        def _button_write_config_clicked(b):
            path = self.module.gradient_boosting.write_config()
            config_path[0].value = 'Config saved to: ' + str(path)

        button_write_config.on_click(_button_write_config_clicked)

        return widgets.VBox([widgets.HBox([button_write_config]), config_path[0]])
