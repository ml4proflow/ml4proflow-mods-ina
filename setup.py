from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="ml4proflow-mods-ina",
    version="0.0.1",
    author="todo",
    author_email="todo",
    description="todo",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # package_dir={"": "src"},
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    namespace_packages=['ml4proflow_mods'],
    data_files=[('ml4proflow/notebooks', ['notebooks/data_segmentation.ipynb'])],
    install_requires=["ml4proflow",
                      "statsmodels",
                      "scipy",
                      "seaborn",  # TODO: only for gui
                      "plotly",  # TODO: only for gui
                      "pmdarima"],  # TODO: only for gui
    extras_require={
        "tests": ["pytest",
                  "pandas-stubs",
                  "pytest-html",
                  "pytest-cov",
                  "flake8",
                  "mypy",
                  "jinja2==3.0.3"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark", "myst-parser", "sphinx-autopi", "sphinx-gallery"],
    },
    python_requires=">=3.6",  # todo
)
